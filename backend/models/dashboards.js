const dashboards = require('../lib/db')('dashboards', true)
const _ = require('lodash')
const log = require('../lib/logger')
const joi = require('joi')

dashboards.ensureIndex({fieldName: 'id_user'})
dashboards.ensureIndex({fieldName: 'is_default'})

module.exports = {
  schema: {
    id_user: joi.string().required(),
    is_default: joi.boolean().required(),
    name: joi.string().empty('')
  },
  model: {
    id_user: null,
    is_default: false,
    name: null
  },

  create (model) {
    return new Promise((resolve, reject) => {
      joi.validate(model, this.schema, {allowUnknown: true}).then(model => {
        try {
          dashboards.update({id_user: model.id_user}, {$set: {is_default: false}}, {multi: true}, (err, affectRows) => {
            if (err) {
              log.error(err)
              return reject(err)
            }
            delete model['_id']
            dashboards.insert(model, (err, dashboard) => {
              if (err) {
                log.error(err)
                return reject(err)
              }

              if (!dashboard) {
                return reject(new Error('Dashboard was not created'))
              }

              resolve(dashboard)
            })
          })
        } catch (err) {
          log.error(err)
          reject(err)
        }
      }).catch(reject)
    })
  },

  update (model) {
    return new Promise((resolve, reject) => {
      if (_.isEmpty(model._id)) return reject(new Error('Missing id dashboard'))

      joi.validate(model, this.schema, {allowUnknown: true}).then(model => {
        try {
          dashboards.update({id_user: model.id_user, _id: model._id}, {$set: model}, {}, (err, affectRows) => {
            if (err) {
              log.error(err)
              return reject(err)
            }

            if (!affectRows) {
              return reject(new Error('Not found dashboard to update'))
            }

            this.getDashboardBy(model._id, model.id_user).then(resolve).catch(reject)
          })
        } catch (err) {
          log.error(err)
          reject(err)
        }
      }).catch(reject)
    })
  },
  removeBy (idDashboard, idUser) {
    return new Promise((resolve, reject) => {
      try {
        if (_.isEmpty(idDashboard)) throw new Error('Missing id dashboard')
        if (_.isEmpty(idUser)) throw new Error('Missing id user')

        dashboards.remove({_id: idDashboard, id_user: idUser}, {}, (err, removedCount) => {
          if (err) {
            log.error(err)
            return reject(err)
          }

          if (!removedCount) return reject(new Error('Dashboard not found'))

          resolve(removedCount)
        })
      } catch (err) {
        log.error(err)
        reject(err)
      }
    })
  },

  getDashboardBy (id, idUser) {
    return new Promise((resolve, reject) => {
      try {
        if (_.isEmpty(id)) throw new Error('Missing required id dashboard')
        if (_.isEmpty(idUser)) throw new Error('Missing required id_user')

        dashboards.findOne({_id: id, id_user: idUser}, (err, dashboard) => {
          if (err) {
            log.error(err)
            return reject(err)
          }

          if (!dashboard) return reject(new Error('Dashboard not found'))

          resolve(dashboard)
        })
      } catch (err) {
        log.error(err)
        reject(err)
      }
    })
  },

  getDefaultDashboardBy (idUser) {
    return new Promise((resolve, reject) => {
      try {
        if (_.isEmpty(idUser)) throw new Error('Missing required id user')

        dashboards.findOne({id_user: idUser, is_default: true}, (err, dashboard) => {
          if (err) {
            log.error(err)
            return reject(err)
          }

          if (!dashboard) {
            this.create({id_user: idUser, is_default: true}).then(dashboard => {
              if (!dashboard) {
                return reject(new Error('Was not posible to create missing default dashboard'))
              }

              resolve(dashboard)
            }).catch(reject)
          } else {
            resolve(dashboard)
          }
        })
      } catch (err) {
        log.error(err)
        reject(err)
      }
    })
  },

  getAllDashboards (idUser) {
    return new Promise((resolve, reject) => {
      try {
        if (_.isEmpty(idUser)) throw new Error('Missing required id_user')

        dashboards.find({id_user: idUser}, (err, dashboards) => {
          if (err) {
            log.error(err)
            return reject(err)
          }

          resolve(dashboards)
        })
      } catch (err) {
        log.error(err)
        reject(err)
      }
    })
  }
}
