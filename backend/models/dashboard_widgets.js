const dashboardWidgets = require('../lib/db')('dashboard_widgets', true)
const _ = require('lodash')
const log = require('../lib/logger')
const joi = require('joi')

dashboardWidgets.ensureIndex({fieldName: 'id_dashboard'})
dashboardWidgets.ensureIndex({fieldName: 'id_user'})

module.exports = {
  schema: {
    id_dashboard: joi.string().required(),
    id_user: joi.string().required()
  },
  model: {
    id_dashboard: null,
    id_user: null
  },
  create (model) {
    return new Promise((resolve, reject) => {
      delete model['_id']
      joi.validate(model, this.schema, {allowUnknown: true})
          .then(model => {
            try {
              dashboardWidgets.insert(model, (err, widget) => {
                if (err) {
                  log.error(err)
                  return reject(err)
                }

                if (!widget) return reject(new Error('Dashboard widget was not created'))

                resolve(widget)
              })
            } catch (err) {
              log.error(err)
              reject(err)
            }
          }).catch(err => {
            log.error(err)
            reject(err)
          })
    })
  },

  update (model) {
    return new Promise((resolve, reject) => {
      joi.validate(model, this.schema, {allowUnknown: true})
          .then(model => {
            try {
              dashboardWidgets.update({_id: model._id, id_user: model.id_user}, {$set: model}, (err, numberOfUpdates) => {
                if (err) {
                  log.error(err)
                  return reject(err)
                }
                if (!numberOfUpdates) reject(new Error('Widget not updated'))

                return this.getWidgetByIdAndIdDashboard(model._id, model.id_dashboard, model.id_user).then(resolve).catch(reject)
              })
            } catch (err) {
              log.error(err)
              reject(err)
            }
          })
          .catch(err => {
            log.error(err)
            reject(err)
          })
    })
  },

  removeBy (idWidget, idDashboard, idUser) {
    return new Promise((resolve, reject) => {
      try {
        if (_.isEmpty(idDashboard)) throw new Error('Missing required id_dashboard')
        if (_.isEmpty(idWidget)) throw new Error('Missing required widget _id')
        if (_.isEmpty(idUser)) throw new Error('Missing required id_user')

        dashboardWidgets.remove({_id: idWidget, id_dashboard: idDashboard, id_user: idUser}, {}, (err, removedCount) => {
          if (err) {
            log.error(err)
            return reject(err)
          }

          if (!removedCount) return reject(new Error('Widget not found'))
          resolve(removedCount)
        })
      } catch (err) {
        log.error(err)
        reject(err)
      }
    })
  },

  getWidgetByIdAndIdDashboard (id, idDashboard, idUser) {
    return new Promise((resolve, reject) => {
      try {
        if (_.isEmpty(id)) throw new Error('Missing required widget Id')
        if (_.isEmpty(idDashboard)) throw new Error('Missing required idDashboard')
        if (_.isEmpty(idUser)) throw new Error('Missing required idUser')

        dashboardWidgets.findOne({_id: id, id_dashboard: idDashboard, id_user: idUser}, (err, dashboardWidgets) => {
          if (err) {
            log.error(err)
            return reject(err)
          }

          resolve(dashboardWidgets)
        })
      } catch (err) {
        log.error(err)
        reject(err)
      }
    })
  },

  getWidgetsByIdDashboard (idDashboard, idUser) {
    return new Promise((resolve, reject) => {
      try {
        if (_.isEmpty(idDashboard)) throw new Error('Missing id dashboard')
        if (_.isEmpty(idUser)) throw new Error('Missing id user')

        dashboardWidgets.find({id_dashboard: idDashboard, id_user: idUser}, {}, (err, widgets) => {
          if (err) {
            log.error(err)
            return reject(err)
          }

          resolve(widgets)
        })
      } catch (err) {
        log.error(err)
        reject(err)
      }
    })
  }
}
