const irpUsers = require('../lib/db')('irp_users', true)
const _ = require('lodash')
const log = require('../lib/logger')
const joi = require('joi')
const dashboards = require('../models/dashboards')
const dashboardWidgets = require('../models/dashboard_widgets')
const irpInstances = require('../models/irp_instances')
const config = require('config')
const utils = require('../lib/utils')
const crypto = require('crypto')

const USERNAME_MIN_LENGTH = 3

module.exports = {
  schema: {
    instance_id: joi.string().trim().required(),
    username: joi.string().trim().regex(/^[0-9a-zA-Z_\.]+$/).lowercase().min(USERNAME_MIN_LENGTH).required(),
    token: joi.string().empty(''),
    creation_date: joi.number().integer().required(),
    status: joi.boolean().required()
  },

  model: {
    instance_id: null,
    username: null,
    token: false,
    creation_date: Date.now(),
    status: true
  },

  getTokenRemeberExpireTime () {
    return utils.getExpireTimeBy(config.TOKEN_REMEMBER_EXPIRE)
  },

  getTokenExpireTime () {
    return utils.getExpireTimeBy(config.TOKEN_EXPIRE)
  },

  create (user) {
    return new Promise((resolve, reject) => {
      joi.validate(user, this.schema).then(user => {
        try {
          delete user['_id']
          irpUsers.insert(user, function (err, user) {
            if (err) {
              log.error(err)
              return reject(err)
            }
            if (!user) return reject(new Error('User insert fail'))
            resolve(user)
          })
        } catch (err) {
          log.error(err)
          reject(err)
        }
      }).catch(err => {
        log.error(err)
        reject(err)
      })
    })
  },

  update (model) {
    return new Promise((resolve, reject) => {
      try {
        if (_.isEmpty(model._id)) throw new Error('Missing required id_user')
        if (model.token) throw new Error('Field token cannot be affected')
        if (model.token_max_age) throw new Error('Field token_max_age cannot be affected')
        if (model.token_remember) throw new Error('Field token_remember cannot be affected')

        irpUsers.update({_id: model._id}, {$set: model}, (err, numberOfUpdates) => {
          if (err) {
            log.error(err)
            return reject(err)
          }

          if (!numberOfUpdates) return reject(new Error('No user to update'))

          this.getUserById(model._id).then(resolve).catch(reject)
        })
      } catch (err) {
        log.error(err)
        reject(err)
      }
    })
  },

  setUpUserToken (idUser, token, maxAge, remembered) {
    return new Promise((resolve, reject) => {
      try {
        if (_.isEmpty(idUser)) throw new Error('Missing required id_user')
        if (_.isEmpty(token)) throw new Error('Field token cannot be empty')
        if (!maxAge) throw new Error('Field token_max_age cannot be empty')

        let tokenSetup = {token: token, token_max_age: maxAge, token_remember: remembered}

        irpUsers.update({_id: idUser}, {$set: tokenSetup}, {}, (err, numberOfUpdates) => {
          if (err) {
            log.error(err)
            return reject(err)
          }

          if (!numberOfUpdates) return reject(new Error('No user to update'))
          irpUsers.persistence.compactDatafile()
          return resolve(tokenSetup)
        })
      } catch (err) {
        log.error(err)
        reject(err)
      }
    })
  },

  removeBy (idUser) {
    return new Promise((resolve, reject) => {
      try {
        if (_.isEmpty(idUser)) throw new Error('Missing id user')

        irpUsers.remove({_id: idUser}, {}, (err, removedCount) => {
          if (err) {
            log.error(err)
            return reject(err)
          }
          if (!removedCount) return reject(new Error('User not found'))

          resolve(removedCount)
        })
      } catch (err) {
        log.error(err)
        reject(err)
      }
    })
  },

  getUserById (id) {
    return new Promise((resolve, reject) => {
      try {
        if (_.isEmpty(id)) throw new Error('Missing required field id user')

        irpUsers.findOne({_id: id}, (err, user) => {
          if (err) {
            log.error(err)
            return reject(err)
          }
          if (!user) return reject(new Error('User not found'))

          resolve(user)
        })
      } catch (err) {
        log.error(err)
        reject(err)
      }
    })
  },

  getUserByUserName (userName) {
    return new Promise((resolve, reject) => {
      try {
        if (_.isEmpty(_.trim(userName))) throw new Error('Missing username')
        userName = userName.toLowerCase()

        irpUsers.findOne({username: userName}, (err, user) => {
          if (err) {
            log.error(err)
            return reject(err)
          }

          if (!user) reject(new Error('User not found'))

          resolve(user)
        })
      } catch (err) {
        log.error(err)
        reject(err)
      }
    })
  },

  async updateToken (user, generateNewToken) {
    let token = user.token

    if (generateNewToken) {
      // generate user token
      token = await new Promise((resolve, reject) => {
        crypto.randomBytes(28, (err, buffer) => {
          if (err) {
            log.error(err)
            return reject(err)
          }

          resolve(buffer.toString('hex'))
        })
      })
    }

    let setupToken = await this.setUpUserToken(
        user._id,
        token,
        Date.now() + (user.token_remember ? this.getTokenRemeberExpireTime() : this.getTokenExpireTime()),
        user.token_remember
    )
    return setupToken
  },

  getAllUsersNotIn (userIds) {
    userIds = (_.isArray(userIds) ? userIds : [])

    return new Promise((resolve, reject) => {
      let extractUserswithNextFields = {first_name: 1, last_name: 1, username: 1, is_admin: 1, email: 1, creation_date: 1}

      try {
        irpUsers.find({_id: {$nin: userIds}}, extractUserswithNextFields, (err, users) => {
          if (err) {
            log.error(err)
            return reject(err)
          }

          if (!users) users = []

          resolve(users)
        })
      } catch (err) {
        log.error(err)
        reject(err)
      }
    })
  },

  profileById (idUser) {
    let self = this
    let profile = {
      user: {},
      dashboard: {
        widgets: []
      },
      instances: []
    }

    return new Promise((resolve, reject) => {
      if (_.isEmpty(idUser)) return reject(new Error('Missing required IdUser'))

      self.getUserById(idUser).then(user => {
        profile.user = _.omit(user, ['password', 'token_max_age', 'token_remember', 'token'])
        Promise.all([dashboards.getDefaultDashboardBy(user._id), irpInstances.getInstancesByIdUser(user._id)])
            .then(result => {
              profile.dashboard = result[0]
              profile.instances = _.map(result[1], instance => {
                return _.omit(instance, 'id_user', 'host')
              })
              dashboardWidgets
                  .getWidgetsByIdDashboard(profile.dashboard._id, user._id)
                  .then(widgets => {
                    profile.dashboard.widgets = widgets
                    resolve(profile)
                  })
                  .catch(err => {
                    log.error(err)
                    reject(err)
                  })
            })
            .catch(err => {
              log.error(err)
              reject(err)
            })
      })
    })
  },
  checkIsValidToken (token) {
    return new Promise((resolve, reject) => {
      try {
        if (_.isEmpty(token)) throw new Error('Missing required field token')

        irpUsers.findOne({token: token, token_max_age: {$gte: Date.now()}}, (err, user) => {
          if (err) {
            log.error(err)
            err.status = 401
            return reject(err)
          }

          if (!user) {
            let error = new Error('User not found')
            error.status = 401
            return reject(error)
          }

          if (user.token_max_age < Date.now()) return reject(new Error('Token expired'))

          resolve(user)
        })
      } catch (err) {
        log.error(err)
        reject(err)
      }
    })
  }
}
