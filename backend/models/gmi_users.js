const bcrypt = require('bcrypt')
const gmiUsers = require('../lib/db')('gmi_users', true)
const _ = require('lodash')
const log = require('../lib/logger')
const joi = require('joi')

gmiUsers.ensureIndex({fieldName: 'username', unique: true})

const USERNAME_MIN_LENGTH = 5

module.exports = {
  schema: {
    is_admin: joi.boolean(),
    first_name: joi.string().trim(),
    last_name: joi.string().trim(),
    username: joi.string().trim().regex(/^[0-9a-zA-Z_\.]+$/).lowercase().min(USERNAME_MIN_LENGTH).required(),
    password: joi.string().trim().min(USERNAME_MIN_LENGTH),
    email: joi.string().email(),
    creation_date: joi.number().integer().required(),
    status: joi.boolean().required()
  },

  model: {
    is_admin: false,
    first_name: null,
    last_name: null,
    username: null,
    password: null,
    email: null,
    creation_date: Date.now(),
    status: true
  },

  create (user) {
    return new Promise((resolve, reject) => {
      joi.validate(user, this.schema).then(user => {
        try {
          gmiUsers.insert(user, function (err, user) {
            if (err) {
              log.error(err)
              return reject(err)
            }
            if (!user) return reject(new Error('User insert fail'))

            resolve(user)
          })
        } catch (err) {
          log.error(err)
          reject(err)
        }
      }).catch(err => {
        log.error(err)
        reject(err)
      })
    })
  },

  update (model) {
    return new Promise((resolve, reject) => {
      try {
        if (_.isEmpty(model._id)) throw new Error('Missing required id_user')

        gmiUsers.update({_id: model._id}, {$set: model}, (err, numberOfUpdates) => {
          if (err) {
            log.error(err)
            return reject(err)
          }

          if (!numberOfUpdates) return reject(new Error('No user to update'))

          this.getUserById(model._id).then(resolve).catch(reject)
        })
      } catch (err) {
        log.error(err)
        reject(err)
      }
    })
  },

  removeBy (idUser) {
    return new Promise((resolve, reject) => {
      try {
        if (_.isEmpty(idUser)) throw new Error('Missing id user')

        gmiUsers.remove({_id: idUser}, {}, (err, removedCount) => {
          if (err) {
            log.error(err)
            return reject(err)
          }
          if (!removedCount) return reject(new Error('User not found'))

          resolve(removedCount)
        })
      } catch (err) {
        log.error(err)
        reject(err)
      }
    })
  },

  getUserById (id) {
    return new Promise((resolve, reject) => {
      try {
        if (_.isEmpty(id)) throw new Error('Missing required field id user')

        gmiUsers.findOne({_id: id}, (err, user) => {
          if (err) {
            log.error(err)
            return reject(err)
          }
          if (!user) return reject(new Error('User not found'))

          resolve(user)
        })
      } catch (err) {
        log.error(err)
        reject(err)
      }
    })
  },

  getUserByUserName (userName) {
    return new Promise((resolve, reject) => {
      try {
        if (_.isEmpty(_.trim(userName))) throw new Error('Missing username')
        userName = userName.toLowerCase()

        gmiUsers.findOne({username: userName}, (err, user) => {
          if (err) {
            log.error(err)
            return reject(err)
          }

          if (!user) reject(new Error('User not found'))

          resolve(user)
        })
      } catch (err) {
        log.error(err)
        reject(err)
      }
    })
  },

  getAllUsersNotIn (userIds) {
    userIds = (_.isArray(userIds) ? userIds : [])

    return new Promise((resolve, reject) => {
      let extractUserswithNextFields = {first_name: 1, last_name: 1, username: 1, is_admin: 1, email: 1, creation_date: 1}

      try {
        gmiUsers.find({_id: {$nin: userIds}}, extractUserswithNextFields, (err, users) => {
          if (err) {
            log.error(err)
            return reject(err)
          }

          if (!users) users = []

          resolve(users)
        })
      } catch (err) {
        log.error(err)
        reject(err)
      }
    })
  },

  authenticate (username, password) {
    log.info('username: %s authenticate request', username)
    return new Promise((resolve, reject) => {
      try {
        if (_.isEmpty(username)) throw new Error('Missing username')
        if (_.isEmpty(password)) throw new Error('Missing password')
        this.getUserByUserName(username)
          .then(user => {
            if (!user) throw new Error('Incorrect username')
            bcrypt.compare(password, user.password, (err, result) => {
              if (err) {
                log.error(err)
                return reject(err)
              }

              if (result === true) {
                return resolve(user)
              }

              resolve(result)
            })
          })
          .catch(err => {
            log.error(err)
            reject(err)
          })
      } catch (err) {
        log.error(err)
        reject(err)
      }
    })
  }
}
