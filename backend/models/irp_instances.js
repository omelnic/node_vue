const _ = require('lodash')
const log = require('../lib/logger')
const irpInstances = require('../lib/db')('irp_instances', true)
const joi = require('joi')

irpInstances.ensureIndex({fieldName: 'host', unique: true})
irpInstances.ensureIndex({fieldName: 'id_user'})

module.exports = {
  schema: {
    id_user: joi.string().trim().required(),
    host: joi.string().trim().required(),
    creation_date: joi.number().required(),
    status: joi.boolean().required()
  },
  model: {
    id_user: null,
    host: null,
    creation_date: Date.now(),
    status: true
  },
  async create (host, idUser) {
    try {
      let instance = {}
      instance.host = host.trim()
      instance.id_user = idUser
      if (_.isEmpty(instance.host)) throw new Error('Missing required host')
      if (_.isEmpty(instance.id_user)) throw new Error('Missing required idUser')
      instance.status = true
      instance.creation_date = Date.now()

      return await new Promise((resolve, reject) => {
        irpInstances.insert(instance, (err, irpInstance) => {
          if (err) return reject(err)
          if (!irpInstance) return reject(new Error('Instance was not inserted'))
          resolve(irpInstance)
        })
      })
    } catch (err) {
      log.error(err)
      throw err
    }
  },
  async update (instance) {
    let self = this
    try {
      let instanceExist = await this.getInstanceById(instance._id)
      if (!instanceExist) throw new Error('Instance does not exists')
      if (instance.host.length && !(_.isEmpty(instance.host)) && instance.hasOwnProperty('host')) {
        instance.host = instance.host.trim()
        instance.status = true
        return await update(instance)
      } else {
        return await update(instance)
      }
    } catch (err) {
      log.error(err)
      throw err
    }
    function update (instance) {
      return new Promise((resolve, reject) => {
        try {
          irpInstances.update({_id: instance._id}, {$set: instance}, (err, numberOfUpdates) => {
            if (err) {
              log.error(err)
              return reject(err)
            }
            if (!numberOfUpdates) {
              return reject(new Error('Not found to update instance'))
            }
            self.getInstanceById(instance._id).then(instance => {
              resolve(instance)
            }).catch(reject)
          })
        } catch (err) {
          log.error(err)
          reject(err)
        }
      })
    }
  },
  removeBy (idInstance) {
    return new Promise((resolve, reject) => {
      if (!idInstance) return reject(new Error('Missing required _id instance'))
      try {
        irpInstances.remove({_id: idInstance}, {}, (err, numberOfRemoved) => {
          if (err) {
            log.error(err)
            return reject(err)
          }
          resolve(numberOfRemoved)
        })
      } catch (err) {
        log.error(err)
        reject(err)
      }
    })
  },
  getInstanceById (id) {
    return new Promise((resolve, reject) => {
      if (_.isEmpty(id)) return reject(new Error('Missing required instance id'))
      try {
        irpInstances.findOne({_id: id}, (err, instance) => {
          if (err) {
            log.error(err)
            return reject(err)
          }
          resolve(instance)
        })
      } catch (err) {
        log.error(err)
        reject(err)
      }
    })
  },
  getInstancesByIdInstances (ids) {
    return new Promise((resolve, reject) => {
      if (_.isEmpty(ids)) return reject(new Error('Missing required instance ids'))
      try {
        // id_user: idUser
        irpInstances.find({_id: {$in: ids}}, (err, instances) => {
          if (err) {
            log.error(err)
            return reject(err)
          }
          resolve(instances)
        })
      } catch (err) {
        log.error(err)
        reject(err)
      }
    })
  }
}
