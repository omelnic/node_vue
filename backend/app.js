const express = require('express')
const path = require('path')
const favicon = require('serve-favicon')
const logger = require('morgan')
const cookieParser = require('cookie-parser')
const bodyParser = require('body-parser')
const history = require('connect-history-api-fallback')
const passport = require('./lib/passport')
const Twig = require("twig")
const compileSass = require('express-compile-sass')
const publicRoot = path.join(__dirname, 'public')
const utils = require('./lib/utils')
const config = require('config')
const forceSSL = require('express-force-ssl')

//routes
const index = require('./routes/index');
const irp = require('./routes/irp');
const gmi = require('./routes/gmi');


const app = express();

// uncomment after placing your favicon in /public
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

// view engine setup
Twig.cache(false)
app.set("twig options", {strict_variables: false})
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'twig')
app.engine('twig', Twig.__express)


//force redirect to https if is prod enveronment
if(config.NODE_ENV === 'production') {
  app.use(forceSSL)
}


//Public resources configure
app.use(compileSass({
	root: publicRoot,
	sourceMap: true, // Includes Base64 encoded source maps in output css
	sourceComments: true, // Includes source comments in output css
	watchFiles: true, // Watches sass files and updates mtime on main files for each change
	logToConsole: false // If true, will log to console.error on errors
}))
app.use(express.static(publicRoot));

//passport require setup
passport.setup(app)

//vue middleware history
app.use(history({
	verbose: true,
	disableDotRule: true,
	rewrites: [
		{from: /\/login/, to: '/login'},
		{from: /\/logout/, to: '/logout'},
		{from: /\/gmi\//, to: function(context) {
				return context.parsedUrl.pathname
			}
		},
		{ from: /\/irp\//, to: function(context) {
				return context.parsedUrl.pathname
			}
		}
	],
	index: '/index'
}));

//route use
app.use('/', index)
app.use('/irp', irp)
app.use('/gmi', gmi)

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  let err = new Error('Page Not Found');
  err.status = 404;
  next(err);
});

//all errors handler
app.use(function(err, req, res, next) {
  if(/application\/json;/.test(req.get('accept'))) {
    res.status(err.status || 500).json(utils.getMessage(true, 'gmi', err.message))
  } else {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};
    // render the error page
    res.status(err.status || 500);
    res.render('error');
  }
});



module.exports = app;
