const debug = require('debug')('backend:server')
const log = require('../lib/logger')
const irpUsers = require('../models/irp_users')
const config = require('config')
const constSocketEvent = require('../config/socket_events')
const utils = require('../lib/utils')
const _ = require('lodash')

module.exports = (options, app) => {
  let isProd = config.NODE_ENV === 'production'
  let port = (isProd ? config.WSS_PORT : config.WS_PORT)
  let path = '/'
  let server = port
  // create https server for diff port of main server
  if (options.hasOwnProperty('key')) {
    const https = require('https')
    server = https.createServer(options).listen(port)
    server.on('error', onError.bind(null, port))
    server.on('listening', onListening.bind(null, server))
  }

  const io = require('socket.io')(server, config.WS_OPTIONS)

  log.info('WEB SOCKET server listen: ' + (isProd ? 'wss://' : 'ws://') + 'localhost:' + port + path)

  let webSocketConnections = {
    clients: {},
    getUserByConnection (idConnection) {
      return (this.clients[idConnection] !== undefined ? this.clients[idConnection] : null)
    },
    getConnectionsByIdUser (idUser) {
      return (this.clients[idUser] !== undefined ? this.clients[idUser] : null)
    },
    removeConnectionsByUserId (idUser) {
      let connections = this.getConnectionsByIdUser(idUser)
      _.each(connections, client => {
        client.disconnect()
        delete webSocketConnections.clients[client.id]
        this.clients[idUser] = []
      })
    }
  }

  io.use((socket, next) => {
   try {
     socket.handshake.headers.cookie = utils.parseCookies(socket.handshake.headers.cookie)
     next()
   } catch (err) {
     next(err)
   }
  })
  io.use((socket, next) => {
    users
        .checkIsValidToken(socket.handshake.headers.cookie['gmi.token'])
        .then(user => {
          if (webSocketConnections.clients[user._id] === undefined || webSocketConnections.clients[user._id] === 'undefined') {
            webSocketConnections.clients[user._id] = []
          }

          webSocketConnections.clients[socket.id] = user
          webSocketConnections.clients[user._id].push(socket)
          log.info(`client connected id_socket ${socket.id} idUser ${user._id}`)
          next()
        })
        .catch(next)
  })
  // Emit message to client
  app.on(constSocketEvent.IRP_INSTANCES_STATUS, message => {
    let connections = webSocketConnections.getConnectionsByIdUser(message.user._id)
    let messageResult = _.cloneDeep(message)
    delete messageResult['user']
    log.info('Total Connections get message: ', connections.length, ' user id: ', message.user._id)
    _.each(connections, socket => {
      socket.emit(constSocketEvent.IRP_INSTANCES_STATUS, messageResult)
    })
  })
  // socket server connections listner
  io.on('connection', (socket) => {
    // Event notification sent to all connections
    app.on(constSocketEvent.GMI_SERVER_NOTIFY, message => {
      socket.emit(constSocketEvent.GMI_SERVER_NOTIFY, message)
    })

    // socket server listner on client disconet
    socket.on('disconnect', () => {
      log.info('User has disconected', socket.id)
      let user = webSocketConnections.getUserByConnection(socket.id)
      if (typeof user === 'object') {
        let connections = webSocketConnections.clients[user._id]
        _.each(connections, client => {
          if (client.id === socket.id) {
            delete webSocketConnections.clients[client.id]
          }
        })
      }
    })
  })
  return io
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError (error, port) {
  if (error.syscall !== 'listen') {
    throw error
  }

  let bind = typeof port === 'string'
      ? 'Pipe ' + port
      : 'Port ' + port

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      log.error(bind + ' requires elevated privileges')
      process.exit(1)
      break
    case 'EADDRINUSE':
      log.error(bind + ' is already in use')
      process.exit(1)
      break
    default:
      throw error
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */
function onListening (server) {
  let addr = server.address()
  let bind = typeof addr === 'string'
      ? 'pipe ' + addr
      : 'port ' + addr.port
  debug('Listening on ' + bind)
}
