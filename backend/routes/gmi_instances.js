const _ = require('lodash')
const bcrypt = require('bcrypt')
const irpInstances = require('../models/irp_instances')
const gmiUsers = require('../models/gmi_users')
const irpUsers = require('../models/irp_users')
const utils = require('../lib/utils')
const irpapid = require('../lib/irpapid')
const passport = require('../lib/passport')

module.exports = function (gmiInstances) {
  gmiInstances.post('/instances', (req, res) => {
    irpInstances
      .create(req.body)
      .then(instance => { res.json(_.omit(instance, ['host', 'id_user'])) })
      .catch(err => {
        res.status(403).json(utils.getMessage(true, 'gmi', err.message))
      })
  })

  gmiInstances.post('/signup', (req, res, next) => {
    console.log('tut')
    let callInstances = []
    _.map(req.body.instances, hostName => {
      callInstances.push(new Promise(resolve => {
        let result = {}
        let call = irpapid.call(hostName)
        call.post('/api/clientLogin', {username: req.body.username, password: req.body.password})
          .then(response => {
            result.responce = response.data
            result.instance = hostName
            resolve(result)
          }).catch(err => {
            console.log(err.message)
            result.responce = utils.getMessage(true, 'irpapid', 'Request fail')
            result.instance = hostName
            resolve(result)
          })
      }))
    })
    Promise.all(callInstances).then(instances => {
      let existToken = false
      for (let i = 0; i < instances.length; i++) {
        if (instances[i].responce.token !== undefined) {
          existToken = true
        }
      }
      if (existToken) {
        gmiUsers.create(
          {
            is_admin: true,
            username: req.body.username,
            password: bcrypt.hashSync(req.body.password, 10),
            creation_date: Date.now(),
            status: true
          }
        ).then(gmiUser => {
          if (!gmiUser) return console.log('User was not inserted')
          Promise.all(instances).then(results => {
            for (let i = 0; i < results.length; i++) {
              irpInstances.create(results[i].instance, gmiUser._id).then(instance => {
                let token = ''
                if (results[i].responce.token !== undefined) {
                  token = results[i].responce.token
                }
                irpUsers.create({
                  instance_id: instance._id,
                  username: req.body.username,
                  token: token,
                  creation_date: Date.now(),
                  status: true
                }).then(irpUser => {}).catch(err => {
                  console.log('error', err)
                })
              }).catch(err => {
                console.log('error', err)
              })
            }
          })
          passport.login(req, res, next)
        }).catch(err => {
          console.log('error', err)
        })
      }
    }).catch(err => {
      res.status(403).json(utils.getMessage(true, 'gmi', err.message))
    })
  })

  gmiInstances.put('/instances/:id', (req, res) => {
    req.body._id = req.params.id
    irpInstances.update(req.body).then(instance => {
      res.json(_.omit(instance, ['host', 'id_user']))
    }).catch(err => {
      res.status(403).json(utils.getMessage(true, 'gmi', err.message))
    })
  })
  gmiInstances.get('/instances/:id', (req, res) => {
    irpInstances
      .getInstanceById(req.params.id).then(instance => {
        res.json(_.omit(instance, ['host', 'id_user']))
      }).catch(err => {
        res.status(403).json(utils.getMessage(true, 'gmi', err.message))
      })
  })
  gmiInstances.delete('/instances/:id', (req, res) => {
    irpInstances
      .removeBy(req.params.id)
      .then(result => { res.json(utils.getMessage(false, 'gmi', 'Instance have been removed')) })
      .catch(err => {
        res.status(403).json(utils.getMessage(true, 'gmi', err.message))
      })
  })
  return gmiInstances
}
