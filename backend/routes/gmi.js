const express = require('express')
const gmi = express.Router()
const utils = require('../lib/utils')

const exceptions = [
  '/signup'
]

gmi.all('/*', (req, res, next) => {
  if (!req.user && !exceptions.includes(req.url)) {
    res.status(401).json(utils.getMessage(true, 'gmi', 'Unauthorised request'))
    return
  }
  next()
})

require('./gmi_users')(gmi)
require('./gmi_instances')(gmi)
require('./gmi_dashboards')(gmi)
require('./gmi_dashboard_widgets')(gmi)

module.exports = gmi
