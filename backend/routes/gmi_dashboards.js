const dashboards = require('../models/dashboards')
const utils = require('../lib/utils')

module.exports = function (gmiDashboard) {
  gmiDashboard.get('/dashboards', (req, res) => {
    dashboards
      .getAllDashboards(req.user._id)
      .then(dashboards => { res.json(dashboards) }).catch(err => {
        res.status(403).json(utils.getMessage(true, 'gmi', err.message))
      })
  })
  gmiDashboard.get('/dashboards/:id', (req, res) => {
    dashboards
      .getDashboardBy(req.params.id, req.user._id)
      .then(dashboard => { res.json(dashboard) })
      .catch(err => {
        res.status(403).json(utils.getMessage(true, 'gmi', err.message))
      })
  })
  gmiDashboard.post('/dashboards', (req, res) => {
    req.body.id_user = req.user._id
    dashboards
      .create(req.body)
      .then(dashboards => { res.json(dashboards) })
      .catch(err => {
        res.status(403).json(utils.getMessage(true, 'gmi', err.message))
      })
  })
  gmiDashboard.put('/dashboards/:id', (req, res) => {
    req.body.id_user = req.user._id
    req.body._id = req.params.id
    dashboards
      .update(req.body)
      .then(dashboard => { res.json(dashboard) })
      .catch(err => {
        res.status(403).json(utils.getMessage(true, 'gmi', err.message))
      })
  })
  gmiDashboard.delete('/dashboards/:id', (req, res) => {
    dashboards
      .removeBy(req.params.id, req.user._id)
      .then(result => {
        res.json(utils.getMessage(false, 'gmi', 'Dashboard removed'))
      })
      .catch(err => {
        res.status(403).json(utils.getMessage(true, 'gmi', err.message))
      })
  })
  return gmiDashboard
}
