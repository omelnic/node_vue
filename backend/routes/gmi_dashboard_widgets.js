const log = require('../lib/logger')
const dashboardWidgets = require('../models/dashboard_widgets')
const utils = require('../lib/utils')

module.exports = (gmiDashboardWidgets) => {
  gmiDashboardWidgets.get('/dashboards/:id/widgets', (req, res) => {
    try {
      dashboardWidgets
          .getWidgetsByIdDashboard(req.params.id, req.user._id)
          .then(widgets => {
            res.json(widgets)
          })
          .catch(err => { throw err })
    } catch (err) {
      log.error(err)
      res.status(403).json(utils.getMessage(true, 'gmi', err.message))
    }
  })

  gmiDashboardWidgets.get('/dashboards/:id/widgets/:idWidget', (req, res) => {
    try {
      dashboardWidgets
          .getWidgetByIdAndIdDashboard(req.params.idWidget, req.params.id, req.user._id)
          .then(widget => {
            res.json(widget)
          })
          .catch(err => { throw err })
    } catch (err) {
      log.error(err)
      res.status(403).json(utils.getMessage(true, 'gmi', err.message))
    }
  })

  gmiDashboardWidgets.post('/dashboards/:id/widgets', (req, res) => {
    try {
      req.body.id_user = req.user._id
      req.body.id_dashboard = req.params.id

      dashboardWidgets
          .create(req.body)
          .then(widget => {
            res.json(widget)
          })
          .catch(err => { throw err })
    } catch (err) {
      log.error(err)
      res.status(403).json(utils.getMessage(true, 'gmi', err.message))
    }
  })

  gmiDashboardWidgets.put('/dashboards/:id/widgets/:idWidget', (req, res) => {
    try {
      req.body._id = req.params.idWidget
      req.body.id_user = req.user._id
      req.body.id_dashboard = req.params.id

      dashboardWidgets
          .update(req.body)
          .then(widget => { res.json(widget) })
          .catch(err => { throw err })
    } catch (err) {
      log.error(err)
      res.status(403).json(utils.getMessage(true, 'gmi', err.message))
    }
  })

  gmiDashboardWidgets.delete('/dashboards/:id/widgets/:idWidget', (req, res) => {
    try {
      dashboardWidgets
          .removeBy(req.params.idWidget, req.params.id, req.user._id)
          .then(widget => {
            res.json(utils.getMessage(false, 'gmi', 'Dashboard widget have been removed'))
          })
          .catch(err => { throw err })
    } catch (err) {
      log.error(err)
      res.status(403).json(utils.getMessage(true, 'gmi', err.message))
    }
  })

  return gmiDashboardWidgets
}
