const express = require('express')
const irp = express.Router()
const irpapid = require('../lib/irpapid')
const utils = require('../lib/utils')

irp.all('/*', (req, res, next) => {
  if (!req.user) {
    res.status(401).json(utils.getMessage(true, 'gmi', 'Unauthorised request'))
    return
  }
  next()
})

// Call API services
irp.get('*', irpapid.requestMiddleware)
irp.post('*', irpapid.requestMiddleware)
irp.put('*', irpapid.requestMiddleware)
irp.patch('*', irpapid.requestMiddleware)
irp.delete('*', irpapid.requestMiddleware)

module.exports = irp
