const express = require('express')
const index = express.Router()
const passport = require('../lib/passport')

index.all('/*', (req, res, next) => {
  if (!req.user && req.url !== '/login' && req.url !== '/setup' && req.url !== '/gmi/signup') {
    res.redirect('/login')
    return
  }
  next()
})

/* GET home page. */
index.get('/index', (req, res, next) => {
  res.render('index')
})

index.get('/login', (req, res, next) => {
  if (req.user) {
    res.redirect('/dashboard')
    return
  }

  res.render('index/login')
})

// index.post('/login', passport.login)
index.get('/logout', passport.logout)

module.exports = index
