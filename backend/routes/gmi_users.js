const _ = require('lodash')
const log = require('../lib/logger')
const irpUsers = require('../models/irp_users')
const utils = require('../lib/utils')

module.exports = (gmiUsers) => {
  gmiUsers.get('/users', (req, res) => {
    try {
      if (!req.user.is_admin) throw new Error('Permission deny')
      irpUsers
        .getAllUsersNotIn([req.user.id])
        .then(users => { res.json(users) })
        .catch(err => {
          res.status(403).json(utils.getMessage(true, 'gmi', err.message))
        })
    } catch (err) {
      log.error(err)
      res.status(403).json(utils.getMessage(true, 'gmi', err.message))
    }
  })
  gmiUsers.get('/users/:id', (req, res) => {
    try {
      if (!req.user.is_admin) throw new Error('Permission deny')
      irpUsers
        .getUserById(req.params.id)
        .then(user => {
          res.json(_.omit(user, ['password', 'token_remember', 'token_max_age', 'token']))
        })
        .catch(err => {
          res.status(403).json(utils.getMessage(true, 'gmi', err.message))
        })
    } catch (err) {
      res.status(403).json(utils.getMessage(true, 'gmi', err.message))
    }
  })
  gmiUsers.post('/users', (req, res) => {
    try {
      if (!req.user.is_admin) throw new Error('Permission deny')
      irpUsers
        .create(req.body)
        .then(user => { res.json(_.pick(user, ['_id', 'first_name', 'last_name', 'username', 'creation_date'])) })
        .catch(err => {
          res.status(403).json(utils.getMessage(true, 'gmi', err.message))
        })
    } catch (err) {
      log.error(err)
      res.status(403).json(utils.getMessage(true, 'gmi', err.message))
    }
  })
  gmiUsers.put('/users/:id', (req, res) => {
    try {
      if (!req.user.is_admin && req.user._id != req.params.id) throw new Error('Permission deny')
      req.body._id = req.params.id
      irpUsers
        .update(req.body)
        .then(user => {
          res.json(user)
        })
        .catch(err => {
          res.json(utils.getMessage(true, 'gmi', err.message))
        })
    } catch (err) {
      log.error(err)
      res.status(403).json(utils.getMessage(true, 'gmi', err.message))
    }
  })
  gmiUsers.delete('/users/:id', (req, res) => {
    try {
      if (!req.user.is_admin && req.user._id != req.params.id) throw new Error('Permission deny')
      if (req.params.id === req.user._id && req.user.is_admin) throw new Error('User of current session cannot be deleted')
      irpUsers.removeBy(req.params.id)
        .then(affect => {
          res.json(utils.getMessage(false, 'gmi', 'User have been deleted!'))
        })
        .catch(err => {
          res.json(utils.getMessage(true, 'gmi', err.message))
        })
    } catch (err) {
      log.error(err)
      res.status(403).json(utils.getMessage(true, 'gmi', err.message))
    }
  })
  gmiUsers.get('/users/:id/profile', (req, res, next) => {
    try {
      if (!req.user.is_admin) throw new Error('Permission deny')
      let idUser = (req.params.id === 'current' ? req.user._id : req.params.id)
      irpUsers
        .profileById(idUser)
        .then(profile => { res.json(profile) })
        .catch(err => {
          res.status(403).json(utils.getMessage(true, 'gmi', err.message))
        })
    } catch (err) {
      log.error(err)
      res.status(403).json(utils.getMessage(true, 'gmi', err.message))
    }
  })
  return gmiUsers
}
