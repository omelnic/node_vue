const config = require('config')
const utils = require('./utils')
const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy
const expressSession = require('express-session')
const log = require('../lib/logger')
const gmiUsers = require('../models/gmi_users')
const irpUsers = require('../models/irp_users')

module.exports = {

  setup (app) {
    app.use(expressSession({
      store: false,
      secret: config.SECRET,
      resave: true,
      saveUninitialized: true
    }))

    app.use(passport.initialize())
    app.use(passport.session())

    // update user token session
    app.use((req, res, next) => {
      if (req.user) {
        res.cookie(
          'gmi.token',
          req.user.token,
          {
            maxAge: irpUsers.getTokenExpireTime()
          }
        )
      } else {
        next()
      }
    })

    passport.use('local', new LocalStrategy({usernameField: 'username', passwordField: 'password'}, (username, password, verified) => {
      gmiUsers
        .authenticate(username, password)
        .then(user => {
          return verified(null, user)
        })
        .catch(err => {
          verified(err, null)
        })
    }))

    passport.serializeUser((user, done) => {
      done(null, user._id)
    })

    passport.deserializeUser((id, done) => {
      gmiUsers.getUserById(id).then(user => {
        done(null, user)
      }).catch(err => {
        done(err, null)
      })
    })
  },
  login (req, res, next) {
    try {
      passport.authenticate('local', async (err, user, info) => {
        if (!err && !user && info) throw new Error(info.message)

        if (err) throw err
        if (!user) {
          let err = new Error('User not found')
          err.status = 401
          throw err
        }

        req.logIn(user, {}, err => {
          if (err) {
            throw err
          }

          if (req.body.remember) {
            req.session.cookie.maxAge = utils.getExpireTimeBy(config.SESSION_REMEMBER_EXPIRE);
          } else {
            // Cookie expire at end of session
            req.session.cookie.expires = false
          }
          res.redirect(req.body.next || '/dashboard')
        })
      })(req, res, next, utils)
    } catch (err) {
      log.error(err)
      next(err)
    }
  },

  logout (req, res, next) {
    if (req.user) {
      req.logout()
    }
    res.redirect('/login')
    return
  }
}
