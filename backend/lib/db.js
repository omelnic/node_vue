const NeDB = require('nedb')

module.exports = (modelName, autoload) => {
  if (!modelName) throw Error('Missing missingModelName')

  return new NeDB({filename: __dirname+'/../../data/'+modelName+'.nedb', autoload: autoload || false});
}