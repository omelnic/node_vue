const axios = require('axios')
const https = require('https')
const utils = require('../lib/utils')
const constSocketEvents = require('../config/socket_events')
const log = require('../lib/logger')

module.exports = {
  call (irpApidHostname) {
    if (!irpApidHostname) throw Error('Missing irpAPIhostname')
    return axios.create({
      baseURL: 'https://' + irpApidHostname,
      headers: {'Content-Type': 'application/json'},
      httpsAgent: new https.Agent({
        rejectUnauthorized: false
      })
    })
  },
  requestMiddleware (req, res, next) {
    try {
      let method = req.method.toLowerCase()
      let body = (req.body ? req.body : {})
      let irpapid = require('./irpapid')
      let irpInstances = require('../models/irp_instances')
      let isVersionRequest = req.url === 'version'
      let requestUri = '/api' + req.url
      let instancesIds = utils.machIRPHostsHeader(req.get('X-IRP-HOSTS'))
      let user = req.user
      if (!instancesIds.length) throw new Error('Missing required instances ids')

      irpInstances.getInstancesByIdInstances(instancesIds)
          .then(instances => {
            if (!instances.length) {
              res.status(403).json(utils.getMessage(true, 'gmi', 'Missing Instances'))
              return
            }
            let callInstances = []
            // create irpapi calls and push together into callInstances
            for (let i = 0; i < instances.length; i++) {
              callInstances.push(new Promise(resolve => {
                let instance = instances[i]
                let callUri = (isVersionRequest ? requestUri : utils.updateQueryStringParameter(requestUri, 'token', instance.token))
                irpapid.call(instance.host)[method](callUri, body)
                    .then(response => {
                      // Send message to websocket server about instance status
                      try {
                        if (instance.status !== true) {
                          instance.status = true
                          irpInstances.update(instance, user._id).catch(err => { log.error(err) })
                        }
                        res.app.emit(constSocketEvents.IRP_INSTANCES_STATUS, {
                          user: user,
                          _id: instance._id,
                          status: 200,
                          is_active: true,
                          message: `Instance ${instance.host} is active`
                        })
                      } catch (err) {
                        log.error(err)
                      }
                      resolve(response.data)
                    }).catch(err => {
                      // Send message to websocket server about instance status
                      let isActive = false
                      if (err.response !== undefined) {
                        isActive = (err.response.hasOwnProperty('status') && err.response.status !== 401 && err.response.status !== 403 && err.response.status !== 200)
                      } else {
                        isActive = (err.hasOwnProperty('status') && err.status !== 401 && err.status !== 403 && err.status !== 200)
                      }
                      try {
                        if (instance.status !== isActive) {
                          instance.status = isActive
                          irpInstances.update(instance, user._id).catch(err => { log.error(err) })
                        }
                        res.app.emit(constSocketEvents.IRP_INSTANCES_STATUS, {
                          user: user,
                          _id: instance._id,
                          status: (err.response.hasOwnProperty('status') ? err.response.status : 503),
                          is_active: isActive,
                          message: (isActive ? `Request to ${instance.host} was not possible to performe` : `Instance ${instance.host} not authorised`)
                        })
                      } catch (e) {
                        log.error(e)
                      }
                      log.error([requestUri, err])
                      resolve(utils.getMessage(true, 'irpapid', 'Request fail'))
                    })
              }))
            }
            // await for all call Instances result at one result
            Promise.all(callInstances).then(results => {
              let instancesResult = {}
              for (let i = 0; i < instances.length; i++) {
                instancesResult[instances[i]._id] = results[i]
              }
              res.json(instancesResult)
            }).catch(err => {
              res.status(403).json(utils.getMessage(true, 'gmi', err.message))
            })
          }).catch(err => { res.status(403).json(utils.getMessage(true, 'gmi', err.message)) })
    } catch (err) {
      log.error([req.url, err])
      res.status(403).json(utils.getMessage(true, 'irpapid', 'Request fail'))
    }
  }
}
