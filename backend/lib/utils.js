module.exports = {
  getMessage (error, component, ...messages) {
    return {error: error, component: component, messages: messages}
  },
  updateQueryStringParameter (uri, key, value) {
    let re = new RegExp('([?&])' + key + '=.*?(&|$)', 'i')
    let separator = uri.indexOf('?') !== -1 ? '&' : '?'
    if (uri.match(re)) {
      return uri.replace(re, '$1' + key + '=' + value + '$2')
    } else {
      return uri + separator + key + '=' + value
    }
  },
  /**
   * Return array
   * @param string
   * @return array
   */
  machIRPHostsHeader (string) {
    return string.split(/[\s,]+/)
  },
  getExpireTimeBy (conf) {
    return (conf.days * (conf.hours * conf.minutes) * (conf.seconds * conf.millisecond))
  },
  parseCookies (cookieString) {
    if (!cookieString || cookieString === undefined) {
      throw new Error('Missing cookieString to parse')
    }
    let list = {}
    let rc = cookieString
    let parts
    rc && rc.split(';').forEach(cookie => {
      parts = cookie.split('=')
      list[parts.shift().trim()] = decodeURI(parts.join('='))
    })
    return list
  }
}
