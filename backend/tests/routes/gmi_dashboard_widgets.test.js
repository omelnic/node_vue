const supertest = require('supertest')
const should = require('should')
const assert = require('assert')
const app = require('../../app')
const mocks = require('../mocks')
const log = require('../../lib/logger')

describe ('test dashboard widgets', () => {
  let httpAgent = supertest.agent(app)
  it ('call login', () => httpAgent
      .post('/login')
      .send(mocks.admin_login)
      .then(res => {
        res.status.should.be.equal(302)
        res.header.location.should.containEql('dashboard')
      }))

  it('should create|update|get|remove dashboard widget', async () => {
    log.info('get user profile')
    let res = await httpAgent.get('/gmi/users/current/profile')
    let dashboard = res.body.dashboard
    res.status.should.be.exactly(200)
    res.body.should.have.property('dashboard').and.not.empty()

    log.info('create widget')
    res = await httpAgent.post(`/gmi/dashboards/${dashboard._id}/widgets`)
        .send({name: 'my widget', id_dashboard: dashboard._id, id_user: dashboard.id_user})
    res.status.should.be.exactly(200)
    res.body.should.have.property('_id').and.not.empty()
    let widget = res.body

    log.info('get widget')
    res = await httpAgent.get(`/gmi/dashboards/${dashboard._id}/widgets/${widget._id}`)
    res.status.should.be.exactly(200)
    res.body.should.have.property('_id').and.not.empty()

    log.info('get all widgets')
    res = await httpAgent.get(`/gmi/dashboards/${dashboard._id}/widgets`)
    res.status.should.be.exactly(200)
    should(res.body.length > 0).and.be.exactly(true)

    log.info('remove created widget')
    res = await httpAgent.delete(`/gmi/dashboards/${dashboard._id}/widgets/${widget._id}`)
    res.status.should.be.exactly(200)
    res.body.should.have.property('error', false)
  })
})
