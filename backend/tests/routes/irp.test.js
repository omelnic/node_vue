const supertest = require('supertest')
const should = require('should')
const assert = require('assert')
const app = require('../../app')
const mocks = require('../mocks')
const log = require('../../lib/logger')

describe('test irpapid call', () => {
  let httpAgent = supertest.agent(app)

  it('call login', function (done) {
    httpAgent
        .post('/login')
        .send(mocks.admin_login)
        .then(res => {
          res.status.should.be.equal(302)
          res.header.location.should.containEql('dashboard')
          done()
        }).catch(err => { log.error(err) })
  })

  it ('call irpapid get currentImprovments', async () => {
    let res = await httpAgent.get('/gmi/instances')

    if (res.body.length) {
      let instances = res.body
      let instancesLength = res.body.length
      let ids = ''
      for (let i = 0; i < instancesLength; i++) {
        ids += (ids ? ',' + instances[i]._id : instances[i]._id)
      }
      res = await httpAgent.get('/irp/reports/currentImprovements?page=1&pageSize=2&sortBy=ts&order=DESC').set('X-IRP-HOSTS', ids)
      res.status.should.be.exactly(200)

      for (let i = 0; i < instancesLength; i++) {
        res.body.should.have.property(instances[i]._id)
      }
    }
  })
})
