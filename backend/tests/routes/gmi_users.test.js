const supertest = require('supertest')
const should = require('should')
const assert = require('assert')
const app = require('../../app')
const mocks = require('../mocks')
const log = require('../../lib/logger')

describe("test Users endpoints",  () => {
  let httpAgent = supertest.agent(app)
  var user = mocks.user


  it("call login", function (done) {
    httpAgent
        .post('/login')
        .send(mocks.admin_login)
        .then(res => {
          res.status.should.be.equal(302)
          res.header.location.should.containEql('dashboard')
          done()
        }).catch(err => {
          log.error(err)
        })
  })

  it("call get /gmi/users ", function (done) {
    httpAgent.get('/gmi/users').set('Accept', 'application/json')
        .then(res => {
          res.status.should.be.equal(200)
          should(res.body.length > 0).be.exactly(true)
          done()
        }).catch(err => {
          log.error(err)
        })
  })

  it("call get /gmi/users/current/profile", function (done) {
    httpAgent.get('/gmi/users/current/profile')
        .then(res => {
          res.status.should.be.exactly(200)
          res.body.should.have.property('user').and.have.an.property('username', mocks.admin_login.username)
          done()
        }).catch(err => {
          log.error(err)
        })
  })

  it('should create|update|get|login|logout|remove test user ',async () => {

    log.info('Insert test User')
    let res = await httpAgent.post('/gmi/users').send(user).set('Accept', 'application/json')
    res.status.should.be.equal(200)
    res.body.should.have.property('username').and.be.exactly(user.username)
    user = {...user, ...res.body}
    user.username = 'test_'+Math.random()

    log.info('update user')
    res = await httpAgent.put('/gmi/users/'+user._id).send({username: user.username})
    res.status.should.be.equal(200)
    res.body.should.have.property('error').and.be.exactly(false)

    log.info('get user')
    res = await httpAgent.get('/gmi/users/'+user._id).set('Accept', 'application/json')
    res.status.should.be.equal(200)
    res.body.should.have.property('_id').and.be.exactly(user._id)

    log.info('login with test user')
    res = await httpAgent.post('/login').send({username: user.username, password: user.password, remember: true, type : 'new_user'})
    res.status.should.be.equal(302)
    res.header.location.should.containEql('dashboard')

    log.info('logout test user')
    res = await httpAgent.get('/logout')
    res.status.should.be.equal(302)
    res.header.location.should.have.containEql('login')

    log.info('login with admin')
    res = await httpAgent.post('/login').send(mocks.admin_login)
    res.status.should.be.equal(302)
    res.header.location.should.containEql('dashboard')

    log.info('remove test user')
    res = await httpAgent.delete('/gmi/users/'+user._id)
    res.status.should.be.equal(200)
    res.body.should.have.property('error').and.be.exactly(false)

    log.info('logout from admin user')
    res = await httpAgent.get('/logout')
    res.status.should.be.equal(302)
    res.header.location.should.have.containEql('login')
  })
})
