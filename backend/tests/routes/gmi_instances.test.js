const supertest = require('supertest')
const should = require('should')
const assert = require('assert')
const app = require('../../app')
const mocks = require('../mocks')
const log = require('../../lib/logger')

describe("test instances endpoints", ()=>{
  let httpAgent = supertest.agent(app)
  let instance = mocks.instance

  it("call login", () =>  httpAgent.post('/login')
      .send(mocks.admin_login)
      .then(res => {
        res.status.should.be.equal(302)
        res.header.location.should.containEql('dashboard')
      }).catch(err => {
        log.error(err)
      }))

  it('should create update get remove instance', async () => {

    log.info('Add instance')
    let res = await httpAgent.post('/gmi/instances').send(instance).set('Accept', 'application/json')
    res.status.should.be.exactly(200)
    res.body.should.have.property('host').and.be.exactly(instance.host)
    instance = res.body

    log.info('Update Instane')
    res = await httpAgent.put(`/gmi/instances/${instance._id}`).send(mocks.instance_update_to).set('Accept', 'application/json')
    res.status.should.be.exactly(200)
    res.body.should.have.property('error').exactly(false)

    log.info('get instnace')
    res = await httpAgent.get(`/gmi/instances/${instance._id}`).set('Accept', 'application/json')
    res.status.should.be.exactly(200)
    res.body.should.have.property('host').and.be.exactly(mocks.instance_update_to.host)

    log.info('get all instances')
    res = await httpAgent.get('/gmi/instances')
    res.status.should.be.exactly(200)
    should(res.body.length > 0).be.exactly(true)

    log.info('delete instance')
    res = await httpAgent.delete('/gmi/instances/'+instance._id).set('Accept', 'application/json')
    res.status.should.be.exactly(200)
    res.body.should.have.property('error').exactly(false)
  })





})