const supertest = require('supertest')
const should = require('should')
const assert = require('assert')
const app = require('../../app')
const mocks = require('../mocks')
const log = require('../../lib/logger')

describe("test dashboard endpoints", () => {
  let httpAgent = supertest.agent(app)

  it("call login", () => httpAgent
      .post('/login')
      .send(mocks.admin_login)
      .then(res => {
        res.status.should.be.equal(302)
        res.header.location.should.containEql('dashboard')
      })
  );

  it('should create|udpate|get|remove|getRemoved dashboard', async () => {
    log.info('Insert dahsboard')
    let res = await httpAgent.post('/gmi/dashboards').send(mocks.dashboard).set('Accept', 'application/json')
    res.status.should.be.exactly(200)
    res.body.should.have.property('_id')
    mocks.dashboard = res.body

    log.info('update dahsboard')
    res = await httpAgent.put('/gmi/dashboards/'+mocks.dashboard._id).set('Accept', 'application/json').send({name: "My First best test dashboard", is_default: true})
    res.status.should.be.exactly(200)
    res.body.should.have.property('error', false)

    log.info('get created dashboard')
    res = await httpAgent.get('/gmi/dashboards/'+mocks.dashboard._id).set('Accept', 'application/json')
    res.status.should.be.exactly(200)
    res.body.should.have.property('_id', mocks.dashboard._id)

    log.info('get all dashboards')
    res = await httpAgent.get('/gmi/dashboards').set('Accept', 'application/json')
    res.status.should.be.exactly(200)
    should(res.body.length > 0).be.exactly(true)

    log.info('delete dashboards')
    res = await httpAgent.delete('/gmi/dashboards/'+mocks.dashboard._id).set('Accept', 'application/json')
    res.status.should.be.exactly(200)
    res.body.should.have.property('error', false)

    log.info('check if fail on removed dashboard')
    res = await httpAgent.get('/gmi/dashboards/'+mocks.dashboard._id).set('Accept', 'application/json')
    res.status.should.be.exactly(200)
    res.body.should.have.property('error', true)
  })
})