const supertest = require('supertest')
const should = require('should')
const assert = require('assert')
const mocks = require('../mocks')
const utils = require('../../lib/utils')

describe ('test lib utils', function () {
  it ('should split hosts', function () {
    should(utils.machIRPHostsHeader(mocks.instances_ids).length === 3).be.exactly(true)
  })
})
