module.exports = {
  admin_login: {username: 'admin', password: 'admin', remember: true},
  user: {
    is_admin: false,
    first_name:"Test",
    last_name:"User",
    username:"test_"+Math.random(),
    password:"test123",
    email:"test@test.local",
    creation_date: Date.now()
  },
  instance: {username: 'admin', password: 'admin', host: 'e3.noction.com'},
  instance_update_to: {username: 'admin', password: 'admin', host: 'e2.noction.com'},
  dashboard: {is_default: true, name: 'My First dashboard'},
  instances_ids: '8763btdfs6t, kjdhsaskd, n9a8d7678s6b'
}