import * as types from '@/store/mutation-types'
import _instances from '../../mocks/instances'

const actionsInjector = require('inject-loader!@/store/modules/instances')

const actions = actionsInjector({
  '../../api/gmi': {
    getInstances () {
      return new Promise((resolve, reject) => {
        setTimeout(() => {
          resolve(_instances)
        }, 100)
      })
    }
  }
})

const testAction = (action, payload, state, expectedMutations, done) => {
  let count = 0

  // mock commit
  const commit = (type, payload) => {
    const mutation = expectedMutations[count]

    try {
      expect(mutation.type).to.equal(type)
      if (payload) {
        expect(mutation.payload).to.deep.equal(payload)
      }
    } catch (error) {
      done(error)
    }

    count++
    if (count >= expectedMutations.length) {
      done()
    }
  }

  // call the action with mocked store and arguments
  action({ commit, state }, payload)

  // check if no mutations should have been dispatched
  if (expectedMutations.length === 0) {
    expect(count).to.equal(0)
    done()
  }
}

describe('actions', () => {
  // > @pnoroc skip till test purpose reveal
  it.skip('getAllInstances', done => {
    testAction(actions.default.actions.getAllInstances, null, {}, [
      { type: types.REQUEST_INSTANCES },
      { type: types.RECEIVE_INSTANCES, payload: { instances: _instances } }
    ], done)
  })
})
