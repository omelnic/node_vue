import InstanceList from '@/components/InstanceList'
import store from '@/store'
import {mount} from 'avoriaz'

describe('Component: InstanceList', () => {
  it('should contain list', () => {
    const vm = mount(InstanceList, {store})

    vm.update()

    expect(vm.vm.$el.querySelectorAll('.instances'))
      .to.have.lengthOf(0)
  })
})
