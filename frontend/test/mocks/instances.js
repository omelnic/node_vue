let nextInstanceId = 1

export default [
  {
    'id': nextInstanceId++,
    'host': 'fe.dev',
    'token': '51f29bca5ed32d5bfe9ad9a06af473d919bac0ee',
    'color': '#92F22A'
  },
  {
    'id': nextInstanceId++,
    'host': 'evaluation.noction.com',
    'byProxy': true,
    'color': '#FD5B03'
  },
  {
    'id': nextInstanceId++,
    'host': 'e1.noction.com',
    'byProxy': true,
    'color': '#D33257'
  },
  {
    'id': nextInstanceId++,
    'host': 'e2.noction.com',
    'byProxy': true,
    'token': '15fa2ac035e1050c114feea4c562c3288c1c2402',
    'color': '#71BA51'
  }
]
