import Vue from 'vue'
import Router from 'vue-router'
import Dashboard from '@/components/dashboard/Dashboard'
import InstanceList from '@/components/InstanceList'
import InstanceForm from '@/components/InstanceForm'
import Profile from '@/components/profile/Profile'
import Report from '@/components/Report'
import NotFoundComponent from '@/components/NotFoundComponent'
import Welcome from '@/components/welcome/Welcome'
import Login from '@/components/auth/Login'
import Setup from '@/components/auth/Setup'
import Loading from '@/components/Loading'
// import store from '@/store/index.js'

Vue.use(Router)

// All routes MUST have name, and filled meta
const router = new Router({
  mode: 'history',
  hashbang: false,
  linkActiveClass: 'active',
  routes: [
    {
      path: '/login',
      name: 'Login',
      component: Login,
      meta: {
        auth: false,
        layoutName: 'clean-layout'
      }
    },
    {
      path: '/setup',
      name: 'Setup',
      component: Setup,
      meta: {
        auth: false,
        layoutName: 'clean-layout'
      }
    },
    {
      path: '/welcome',
      name: 'Welcome',
      component: Welcome,
      meta: {
        auth: true,
        menu: false
      }
    },
    {
      path: '/dashboard',
      name: 'Dashboard',
      component: Dashboard,
      meta: {
        auth: true,
        menu: true,
        icon: 'icon-dash'
      }
    },
    {
      path: '/reports',
      name: 'Reports',
      component: Report,
      meta: {
        auth: true,
        menu: true,
        icon: 'icon-commit'
      },
      children: [
        {
          path: '/reports/current-improvements',
          name: 'ReportCurrentImprovements',
          component: Report,
          auth: true
        },
        {
          path: '/reports/as-pattern-problems',
          name: 'ReportAsPatternProblems',
          component: Report,
          auth: true,
          children: [
            {
              path: '/reports/as-pattern-problems/prefixes',
              name: 'ASPatternProblemPrefixes',
              component: Report
            }
          ]
        }
      ]
    },
    {
      path: '/settings',
      name: 'settings',
      component: Profile,
      menu: true,
      icon: 'icon-cfg',
      meta: {
        auth: true,
        menu: true,
        icon: 'icon-cfg'
      }
    },
    {
      path: '/settings/instances',
      name: 'Instances',
      component: InstanceList,
      meta: {
        auth: true,
        menu: false
      },
      children: [
        {
          path: '/instance/:instanceId',
          name: 'InstanceForm',
          component: InstanceForm,
          meta: {
            auth: true,
            menu: false
          }
        }
      ]
    },
    {
      path: '/',
      name: 'Loading',
      component: Loading
    },
    {
      path: '*',
      component: NotFoundComponent,
      meta: {
        auth: true
      }
    }, {
      path: '/404',
      name: 'error-404',
      component: require('./../components/pages/404.vue')
    }, {
      path: '/403',
      name: 'error-403',
      component: require('./../components/pages/403.vue')
    }, {
      path: '/502',
      name: 'error-502',
      component: require('./../components/pages/502.vue')
    }
  ]
})

// router.beforeEach((to, from, next) => {
//   console.log('front user', this.$auth)
//   if (!to.user && (to.fullPath !== '/login' && to.fullPath !== '/setup')) {
//     window.location.href = '/login'
//   }
//   if (to.matched.some(record => record.meta.requiresAuth) && !store.getters.isLogged) {
//     if (to.name !== 'Loading' || !to.name) {
//       next({
//         path: '/'
//       })
//     }
//   } else {
//     next()
//   }
//   if (to.name === 'notfound' && store.getters.isLogged) {
//     next({name: 'Dashboard'})
//   } else if (to.name === 'notfound' && !store.getters.isLogged) {
//     window.location.href = '/login'
//   }
// })

export default router
