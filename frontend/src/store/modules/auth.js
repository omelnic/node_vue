import gmiApi from '../../api/gmi'
import * as types from '../mutation-types'

const state = {
  hosts: [],
  username: ''
}

// getters
const getters = {
  // dashboardWidgets: state => state.widgets,
  // dashboardTitle: state => state.title,
  // dashboardColumns: state => Number(state.columns),
  // defaultDashboard: state => state
}

// actions
const actions = {
  validateNewCustomer ({commit}, {username, password, instances}) {
    return gmiApi.signUp(username, password, instances)
      .then(({instances}) => commit(types.ADD_INSTANCES, instances))
  }
}
// mutations
const mutations = {
}

export default {
  state,
  getters,
  mutations,
  actions
}
