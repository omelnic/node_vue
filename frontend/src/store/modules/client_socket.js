 const state = {
   socket: {
     connect: false,
     message: null
   }
 }

 const getters = {}

 const actions = {}

 const mutations = {
   SOCKET_CONNECT: (state) => {
     state.socket.connect = true
   },
   SOCKET_CONNECT_ERROR: (state) => {
     state.socket.connect = false
   },
   SOCKET_ERROR: (state) => {
     state.socket.connect = false
   },
   SOCKET_RECONNECT_ERROR: (state, error) => {
     state.socket.connect = false
   },
   SOCKET_RECONNECT_FAILED: (state, error) => {
     state.socket.connect = false
   },
   SOCKET_DISCONNECT: (state) => {
     state.socket.connect = false
   }
 }

 export default {
   state,
   getters,
   actions,
   mutations
 }
