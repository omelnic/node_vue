import md5 from 'js-md5'
import _ from 'lodash'
import Vue from 'vue'

const MUTATION_TYPES = {
  NOTIFY: 'NOTIFY'
}

const REPEAT_NOTIFY_MESSAGE_AFTER_MINUTES = 5

const state = {
  list: {},
  previous_notification: {},
  current_notification: {}
}

const actions = {
  notify ({commit}, options = {type: '', title: '', text: '', group: ''}) {
    commit(MUTATION_TYPES.NOTIFY, options)
  }
}

const getters = {
  getCurrentNotification: state => {
    return state.current_notification
  },
  getPreviouseNotification: state => {
    return state.previous_notification
  },
  getNotifications: state => {
    _.each(state.list, (entry, key) => {
      let minuteOld = Math.ceil((((Date.now() - entry.creation_timestamp) / 1000) / 60))
      if (minuteOld >= REPEAT_NOTIFY_MESSAGE_AFTER_MINUTES) {
        delete state.list[key]
      }
    })
  }
}

const mutations = {
  NOTIFY: notify,
  SOCKET_GMI_SERVER_NOTIFY: (state, data) => {
    notify(state, {text: data.message || 'Server not availble', type: (data.status > 200 ? 'error' : ''), _id: data._id})
  },
  SOCKET_IRP_INSTANCES_STATUS: (state, data) => {
    notify(state, {text: data.message, type: (data.status > 200 ? 'error' : ''), _id: data._id})
  }
}

function notify (state, data) {
  let uniqueMessageid = md5(`${data.message}_${data._id}`)
  let notification = state.list[uniqueMessageid]
  let minuteOld = 0
  if (typeof notification === 'object') {
    minuteOld = Math.ceil((((Date.now() - notification.creation_timestamp) / 1000) / 60))
  }
  if (minuteOld >= REPEAT_NOTIFY_MESSAGE_AFTER_MINUTES || minuteOld === 0) {
    state.previous_notification = state.current_notification
    state.current_notification = data
    Vue.set(state.list, uniqueMessageid, {...data, creation_timestamp: Date.now()})
  }
}

export default {
  state,
  actions,
  getters,
  mutations
}
