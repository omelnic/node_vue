import Vue from 'vue'
import gmiApi from '../../api/gmi'
import IrpApi from '../../api/irp'
import * as types from '../mutation-types'
import _ from 'lodash'

// initial state
const state = {
  all: [],
  states: {},
  loaded: false,
  socket: {
    message: null
  }
}

// getters
const getters = {
  allInstances: state => state.all,
  getInstanceById: (state, getters) => id => state.all.find(instance => instance._id === id),
  getInstanceState: (state, getters) => id => state.states[id]
}

// actions
const actions = {
  async getAllInstances ({ commit, dispatch, state }) {
    let response = await gmiApi.getInstances()
    let instances = response.data

    commit(types.RECEIVE_INSTANCES, instances)
    dispatch('components', _.map(instances, instance => {
      return instance._id
    }))
    return {response, instances}
  },

  removeInstance ({ commit, getters }, instance) {
    return gmiApi.removeInstance(instance)
      .then(() => commit(types.REMOVE_INSTANCE, instance))
  },
  addInstance ({commit}, instance) {
    return gmiApi.addInstance(instance)
      .then(({instance}) => commit(types.ADD_INSTANCE, instance))
  },
  saveInstance: function ({commit}, instance) {
    return gmiApi.saveInstance(instance)
      .then(({instance}) => {
        commit(types.UPDATE_INSTANCE, instance)
        return instance
      })
  },
  version: function ({commit, getters}, instances) {
    let update = (response, id) => {
      let params = {
        version: _.result(response, 'version.version'),
        instance: getters.getInstanceById(id)
      }
      commit(types.VERSION_INSTANCE, params)
    }
    return new IrpApi(instances).get('/irp/version')
      .then((response) => { _.each(response.data, update) })
  },
  // > Expect array of instance or instances id
  components: function ({commit, getters}, instance) {
    if (!_.every(instance, _.isString)) {
      console.error('Wrong instances parameter specified', instance)
      return new Promise().reject()
    }
    let update = (response, id) => {
      let params = {
        components: _.result(response, 'status.components'),
        instance: getters.getInstanceById(id)
      }
      commit(types.UPDATE_INSTANCE_COMPONENTS_STATUS, params)
    }
    return new IrpApi(instance).get('/irp/status/components')
      .then((response) => { _.each(response.data, update) })
  }
}

// mutations
const mutations = {
  [types.REQUEST_INSTANCES] (state) {
    state.loaded = false
  },

  [types.RECEIVE_INSTANCES] (state, instances) {
    state.all = instances
    state.loaded = true
  },

  [types.ADD_INSTANCE] (state, instance) {
    const foundInstance = state.all.find(iter => iter._id === instance._id)
    if (!foundInstance) {
      state.all.push(instance)
    }
  },

  [types.ADD_INSTANCES] (state, instances) {
    state.all = instances
  },

  [types.ADD_INSTANCE_FAILURE] (state, instance) {
    // const foundInstance = state.all.find(iter => iter.id === instance.id)
  },
  [types.UPDATE_INSTANCE] (state, instance) {
    state.all.find((currentInstance, key) => {
      if (currentInstance._id === instance._id) {
        Vue.set(state.all, key, instance)
      }
    })
  },
  [types.REMOVE_INSTANCE] (state, instance) {
    state.all.splice(state.all.indexOf(state.all.find(iter => iter._id === instance.id)), 1)
  },
  [types.UPDATE_INSTANCE_STATE] (state, id, status) {
    state.states[id] = status
  },
  [types.CLEAR_INSTANCE_STATE] (state, id) {
    delete state.states[id]
  },
  [types.LOGIN_INSTANCE] (state, { instance }) {
    Vue.set(instance, 'authorized', true)
  },

  [types.LOGIN_INSTANCE_FAILURE] (state, { instance }) {
    Vue.set(instance, 'error', true)
  },

  [types.STATUS_INSTANCE] (state, { instance, status }) {
    Vue.set(instance, 'status', status)
  },

  [types.STATUS_INSTANCE_FAILURE] (state, { instance }) {
    Vue.set(instance, 'error', true)
  },

  [types.VERSION_INSTANCE] (state, { instance, version }) {
    instance.version = version
  },

  [types.VERSION_INSTANCE_FAILURE] (state, { instance }) {
    Vue.set(instance, 'error', true)
  },
  SOCKET_IRP_INSTANCES_STATUS: (state, res) => {
    state.all.find(instance => {
      if (instance._id === res._id) {
        Vue.set(instance, 'status', res.is_active)
      }
    })
  },
  [types.UPDATE_INSTANCE_COMPONENTS_STATUS] (state, {instance, components}) {
    Vue.set(instance, 'components', components)
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
