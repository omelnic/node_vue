import _ from 'lodash'
import axios from 'axios'

// initial state
const state = {
  first_name: '',
  last_name: '',
  username: '',
  email: '',
  loaded: false
}

// getters
const getters = {
  name: state => '{first_name} {last_name}'.format(state),
  email: state => state.email,
  profile: state => state,
  isLogged: (state) => {
    return state.loaded
  }
}

// mutations
const mutations = {
  'UPDATE_PROFILE' (state, profile) {
    state = _.extend(state, profile)
  },
  'UPDATE_LOGGED_STATE' (state, status) {
    state.loaded = status
  }
}

const actions = {
  async user (context) {
    return new Promise((resolve) => {
      axios.get('/gmi/users/current/profile').then((response) => {
        context.commit('UPDATE_PROFILE', response.data.user)
        context.commit('UPDATE_LOGGED_STATE', _.isObject(response.data.user))
        resolve(response)
      }).catch(() => { window.location.login = '/login' })
    })
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
