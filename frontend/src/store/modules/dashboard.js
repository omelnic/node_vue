import _ from 'lodash'
import gmiApi from '../../api/gmi'
import * as Types from '../mutation-types'

const state = {
  title: '',
  columns: 2,
  widgets: [],
  filters: []
}

// getters
const getters = {
  dashboardWidgets: state => state.widgets,
  dashboardTitle: state => state.title,
  dashboardColumns: state => Number(state.columns),
  defaultDashboard: state => state
}

// actions
const actions = {
  createWidget ({commit}, {dashboard, widget}) {
    return gmiApi.addWidget(dashboard, widget)
      .then(() => commit(Types.DASHBOARD_ADD_WIDGET, widget))
  },
  updateWidget ({commit}, {dashboard, widget}) {
    return gmiApi.updateWidget(dashboard, widget)
      .then(() => {
        commit(Types.DASHBOARD_UPDATE_WIDGET, widget)
        return {dashboard, widget}
      })
  },
  deleteWidget ({commit}, {dashboard, widget}) {
    return gmiApi.deleteWidget(dashboard, widget)
      .then(({res, widget}) => {
        commit(Types.DASHBOARD_REMOVE_WIDGET, widget)
        return {res, widget}
      })
  }
}

// mutations
const mutations = {
  [Types.DASHBOARD_ADD_WIDGET] (state, widget) {
    return state.widgets.push(widget)
  },
  [Types.DASHBOARD_CHANGE_TITLE] (state, title) {
    state.title = String(title)
  },
  [Types.DASHBOARD_CHANGE_COLUMNS] (state, number) {
    state.columns = Number(number)
  },
  [Types.DASHBOARD_INIT] (state, dashboard) {
    state = _.extend(state, dashboard)
  },
  [Types.DASHBOARD_UPDATE_WIDGET] (state, widget) {
    state.widgets.find(dashboardWidget => {
      if (dashboardWidget._id === widget._id) {
        Object.assign(dashboardWidget, widget)
        return true
      }
      return false
    })
  },
  [Types.DASHBOARD_REMOVE_WIDGET] (state, widget) {
    let indexOf = state.widgets.indexOf(state.widgets.find(iter => iter._id === widget._id))
    if (indexOf !== -1) {
      state.widgets.splice(indexOf, 1)
    }
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
