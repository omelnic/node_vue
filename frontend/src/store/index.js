import Vue from 'vue'
import Vuex from 'vuex'
import instances from './modules/instances'
import user from './modules/user'
import dashboard from './modules/dashboard'
import clientSocket from './modules/client_socket'
import notifications from './modules/notifications'
import auth from './modules/auth'

Vue.use(Vuex)
export const strict = false
export default new Vuex.Store({
  modules: {
    user,
    instances,
    dashboard,
    clientSocket,
    notifications,
    auth
  },
  strict: !Vue.config.is_prod
})
