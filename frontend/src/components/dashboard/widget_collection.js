import _ from 'lodash'

const DefaultsWidgetStruct = {
  slug: '',
  title: '',
  description: '',
  settings: {},
  viewName: '',
  refreshable: true
}

const PreDefinedWidgets = [
  {
    slug: 'currentImprovements',
    title: 'Current Improvements',
    description: 'Displays a summary of improved prefixes based on the Improved Prefixes report',
    settings: {
      RefreshOptions: 60,
      IpAllOptions: 0,
      Instances: null,
      PerInstanceOptions: 5
    },
    // > Name of tag
    viewName: 'current-improvements',
    refreshable: true
  },
  {
    slug: 'asPatternProblems',
    title: 'AS-path problems',
    description: 'The "AS Pattern Problems" report contains all the as-patterns identified as problematic by IRP.',
    viewName: 'as-pattern-problems',
    settings: {
      RefreshOptions: 60,
      Instances: null,
      PerInstanceOptions: 5,
      TimestampInterval: 24
    },
    refreshable: true
  },
  {
    slug: 'platformStatus',
    title: 'Platform status',
    viewName: 'platform-performance',
    description: 'Displays status of IRP modules such as collector span/flow, BGPd, etc.',
    settings: {
      RefreshOptions: 60,
      IpOptions: 4,
      Instance: null
    },
    refreshable: true
  },
  {
    slug: 'latencyDestinations',
    title: 'Latency destinations',
    viewName: 'latency-destinations',
    description: 'Displays the amount of destinations with latency being improved',
    settings: {
      RefreshOptions: 60,
      Instance: null,
      TimestampInterval: 24
    },
    refreshable: true
  },
  {
    slug: 'latencyImprovements',
    title: 'Latency Improvements',
    viewName: 'latency-improvements',
    description: 'Displays the Latency values for improvments and and reamining Latencty',
    settings: {
      RefreshOptions: 60,
      Instance: null,
      TimestampInterval: 24
    },
    refreshable: true
  },
  {
    slug: 'probesPerProvider',
    title: 'Probes per provider',
    description: 'Displays the probes per provider',
    viewName: 'probe-per-provider',
    settings: {
      RefreshOptions: 60,
      Instance: null
    },
    refreshable: true
  },
  {
    slug: 'providersPerformance',
    title: 'Providers Performance',
    description: 'Displays the provider Performance',
    viewName: 'providers-performance',
    settings: {
      RefreshOptions: 60,
      Instance: null
    },
    refreshable: true
  }, {
    slug: 'probedPrefixesImprovments',
    title: 'Probed prefixes and Improvements',
    description: 'Displays the Probed prefixes and Improvements',
    viewName: 'probed-prefixes-improvments',
    settings: {
      RefreshOptions: 60,
      Instance: null,
      IpOptions: 4,
      TimestampInterval: 4
    },
    refreshable: true
  },
  {
    slug: 'instancesStatus',
    title: 'Instances status',
    description: 'Show list of configured IRP api instances with there statuses',
    viewName: 'instances-status',
    settings: {
      RefreshOptions: 60,
      Instances: null
    },
    refreshable: true
  },
  {
    slug: 'instanceComponents',
    title: 'Instance components status',
    description: 'Displays instance components status, with there status',
    settings: {
      RefreshOptions: 60,
      Instance: null,
      ComponentsOptions: 'Irpspand'
    },
    // > Name of tag
    viewName: 'instance-parts-status',
    refreshable: true
  }
  // , fixme next widget have been disabled because is missing realisation on irpapid endpoint to {/api/reports/providerPerformanceHistory}
  // {
  //   slug: 'providerPerformanceHistory',
  //   title: 'Provider performance history',
  //   description: 'Description Provider performance history',
  //   viewName: 'provider-performance-history',
  //   settings: {
  //     RefreshOptions: 60,
  //     Instance: null,
  //     TimestampInterval: 4,
  //     Region: 'total',
  //     ProblemType: 'sumloss'
  //   }
  // }
]

const WidgetsCollection = _.reduce(PreDefinedWidgets, function (memo, defintion) {
  memo.push(_.merge(_.cloneDeep(DefaultsWidgetStruct), _.cloneDeep(defintion)))
  return memo
}, [])

export default {
  WidgetsCollection
}
