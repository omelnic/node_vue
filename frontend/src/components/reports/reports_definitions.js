export default {
  'currentImprovements': {
    title: 'Current Improvements',
    endpoint: '/irp/reports/currentImprovements',
    columns: [
      {key: 'index', label: '#', width: '1%', center: true},
      {key: 'ts', label: 'Time', width: '1%', center: false, filters: ['pritifyDate'], sortable: true},
      {key: 'prefix', label: 'Prefix', width: '1%', center: false, sortable: true},
      {key: 'asnumber', label: 'Asn', width: '10px', center: true, sortable: true},
      {key: 'asname', label: 'As Name', width: '10px', center: false, sortable: true},
      {key: 'details', width: '10px', center: false, sortable: false},
      {key: 'providerfrom', label: 'From', width: '5%', center: true, sortable: true},
      {key: 'providerto', label: 'To', width: '5%', center: true, sortable: true},
      {key: 'type', width: '5%', center: true, type: 'icon', sortable: true},
      {key: 'rd', width: '5%', center: true, sortable: true}
    ],
    filters: {
      pageSize: 20,
      page: 1,
      sortBy: 'ts',
      order: 'DESC',
      prefix: null,
      asnumber: null,
      asnname: null,
      from: null,
      to: null
    }
  },
  'historicalRecords': {
    title: 'Historical records',
    endpoint: '/reports/historicalRecords',
    columns: [
      {key: 'index', label: '#', width: '1%', center: true},
      {key: 'ts', label: 'Time', width: '1%', center: false, filters: ['pritifyDate'], sortable: true},
      {key: 'prefix', label: 'Prefix', width: '1%', center: false, sortable: true},
      {key: 'asnumber', label: 'Asn', width: '10px', center: true, sortable: true},
      {key: 'asname', label: 'As Name', width: '10px', center: false, sortable: true},
      {key: 'type', width: '5%', center: true, type: 'icon', sortable: true},
      {key: 'rd', width: '5%', center: true, sortable: true}
    ],
    filters: {
      pageSize: 20,
      page: 1,
      sortBy: 'ts',
      order: 'DESC',
      prefix: null,
      asnumber: null,
      asnname: null
    }
  },
  'ASPatternProblems': {
    title: 'Congestion and outages',
    endpoint: '/irp/reports/ASPatternProblems',
    columns: [
      {key: 'color', label: 'Host', width: '10%', center: true, type: 'color'},
      {key: 'aspattern', label: 'AS-Pattern', width: '10%', center: true},
      {key: 'problem', label: 'Problem', width: '10%', filters: ['uppercaseFirstChar'], center: true},
      {key: 'ts', label: 'Time', width: '10%', center: false, filters: ['pritifyDate'], sortable: true},
      {
        label: 'Affected prefixes',
        type: 'group',
        center: true,
        width: '10%',
        columns: [
          {key: 'total', label: 'Total', width: '10%', center: true},
          {key: 'selectedforprobing', label: 'Selected for Probing', width: '10%', center: true},
          {key: 'probed', label: 'Probed', width: '10%', center: true},
          {key: 'pendingprobing', label: 'Pending probing', width: '10%', center: true},
          {key: 'problemconfirmed', label: 'Problem Confirmed', width: '10%', center: true},
          {key: 'problemnotconfirmed', label: 'Problem not confirmed', width: '10%', center: true},
          {key: 'confirmationrate', label: 'Confirmation rate', width: '10%', center: true}
        ]
      },
      {
        key: 'affectedPrefixes',
        label: 'Problem',
        type: 'group',
        center: true,
        width: '10%',
        columns: [
          {key: 'problemstatus', label: 'Status', width: '1%', center: true, type: 'icon'}
        ]
      }
    ],
    filters: {
      start: null,
      end: null,
      pageSize: 20,
      page: 1,
      sortBy: 'ts',
      order: 'DESC',
      prefix: null,
      from: null,
      to: null
    }
  },
  'ASPatternProblemPrefixes': {
    title: 'Congestion and outages',
    endpoint: '/irp/reports/ASPatternProblemPrefixes',
    columns: [
      {key: 'prefix', label: 'Prefix', width: '10%', center: true}
    ],
    filters: {
      start: null,
      end: null,
      pageSize: 20,
      page: 1,
      sortBy: 'ts',
      order: 'DESC',
      prefix: null,
      from: null,
      to: null
    }
  },
  'ASPatternProblemPrefixesState': {
    title: 'Congestion and outages',
    endpoint: '/irp/reports/ASPatternProblemPrefixes',
    columns: [
      {key: 'prefix', label: 'Prefix', width: '10%', center: true},
      {key: 'prefixstate', label: 'Prefix State', width: '10%', filters: ['uppercaseFirstChar'], center: true}
    ],
    filters: {
      start: null,
      end: null,
      pageSize: 20,
      page: 1,
      sortBy: 'ts',
      order: 'DESC',
      prefix: null,
      from: null,
      to: null
    }
  }
}
