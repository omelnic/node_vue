import _ from 'lodash'
export default class PieInPie {
  PieChart
  canvas
  constructor (canvas, options) {
    this.canvas = canvas
    this.setupCanvasSizes()
    this.PieChart = new PieChart(this.canvas, options)
  }
  draw () {
    this.PieChart.draw()
    this.onResize()
  }
  onResize () {
    window.addEventListener('resize', ev => {
      this.setupCanvasSizes()
      this.PieChart.updateLayout(this.canvas)
      this.PieChart.draw()
    }, false)
  }
  setupCanvasSizes () {
    let parent = this.canvas.closest('div')
    if (parent !== null) {
      this.canvas.setAttribute('width', parent.offsetWidth + 'px')
      this.canvas.setAttribute('height', parent.offsetWidth / 2 + 'px')
    }
  }
}

export class PieChart {
  canvas
  ctx
  hitCanvas
  hitMatrixCtx
  datasets = []
  layout = {}
  lines = []
  pieSlices = []
  labelStyles = {
    color: '#262626',
    font_family: 'Arial',
    font_size: 10
  }
  legendStyles = {
    color: '#5d5d5d',
    font_family: 'Arial',
    font_size: 10
  }
  labelDescriptionStyles = {
    color: '#000000',
    font_family: 'Arial',
    font_size: 10
  }
  Utils
  unknownMatrixObjects = []
  matrixObjects = []
  activeObject
  constructor (canvas, options) {
    this.Utils = ChartUtils
    this.validate(canvas, options)
    this.canvas = canvas
    this.hitCanvas = this.canvas.cloneNode()
    this.ctx = this.canvas.getContext('2d')
    this.hitMatrixCtx = this.hitCanvas.getContext('2d')
    this.datasets = options.datasets
    this.labelStyles = (typeof options.labelStyles === 'object' ? _.extends(this.labelStyles, options.labelStyles) : this.labelStyles)
    this.updateLayout(canvas)
    this.events()
  }
  validate (canvas, options) {
    if (!canvas) {
      throw new Error('Missing canvas')
    }
    if (!options.datasets) {
      throw new Error('Missing datasets')
    }
  }
  updateLayout (canvas) {
    this.layout = {
      width: canvas.width,
      height: canvas.height
    }
  }
  events () {
    this.canvas.addEventListener('mousemove', event => {
      this.drawOnEvent(event)
    })
    this.canvas.addEventListener('click', event => {
      this.drawOnEvent(event)
    })
  }
  drawOnEvent (event) {
    const mousePos = {
      x: event.offsetX,
      y: event.offsetY
    }
    // get pixel under cursor
    const pixel = this.hitMatrixCtx.getImageData(mousePos.x, mousePos.y, 1, 1).data
    const color = `rgb(${pixel[0]},${pixel[1]},${pixel[2]})`

    if (this.activeObject) {
      this.activeObject = null
      this.draw()
    }
    let index = 0
    this.matrixObjects.forEach(object => {
      if (object.key === color) {
        this.matrixObjects = []
        this.activeObject = {object, index}
        this.draw()
        this.drawLabelDescriptions({ctx: this.ctx, index: index})
      }
      index++
    })
  }
  /**
   * Draw
   */
  draw () {
    this.pieSlices = []
    let offset = (this.layout.width / 2.5)
    let contexts = [{ctx: this.ctx, matrix: false}, {ctx: this.hitMatrixCtx, matrix: true}]
    contexts.forEach(canvas => {
      canvas.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height)
      this.drawPie(canvas, this.datasets[0], -offset)
      this.drawLines(canvas)
      this.drawPie(canvas, this.datasets[1], offset)
      this.drawLegend(canvas)
      if (!canvas.matrix) {
        // skip draw on matrix
        this.drawPieSliceLabel(canvas)
      }
    })
  }

  drawPie (canvas, pie, offset = 0) {
    let total = pie.data.reduce((a, b) => (a + b), 0)
    let centerX = (this.layout.width + offset) / 2
    let centerY = (this.layout.height) / 2
    let radius = Math.min(centerX / 2, centerY / 2)
    let startAngle = 0
    let endAngle
    let color
    let alphaColor
    pie.data.forEach((value, index) => {
      color = pie.colors[index]
      let sliceAngle = (2 * Math.PI) * (value / total)
      startAngle = (!startAngle ? (Math.PI * 2 - sliceAngle) / 2 : startAngle)
      endAngle = startAngle + sliceAngle
      color = (canvas.matrix ? this.Utils.getRandomColor() : color)
      if (this.activeObject && this.activeObject.object.color === color) {
        alphaColor = this.Utils.hexToRgb(pie.colors[index])
        alphaColor = `rgba(${alphaColor.r},${alphaColor.g},${alphaColor.b},0.8)`
        this.drawPieSlice(canvas, centerX, centerY, radius, startAngle, endAngle, alphaColor)
      } else {
        this.drawPieSlice(canvas, centerX, centerY, radius, startAngle, endAngle, color)
      }
      this.addObject({type: 'pie', x: centerX, y: centerY, radius, start_angle: startAngle, end_angle: endAngle, color: pie.colors[index], key: color}, canvas.matrix)
      this.defineLines(offset, index, centerX, radius, startAngle, centerY, endAngle)
      this.pieSlices.push({
        center_x: centerX,
        center_y: centerY,
        radius: radius,
        slice_angle: sliceAngle,
        start_angle: startAngle,
        end_angle: endAngle,
        color: color,
        value: value,
        improved_value: pie.values[index],
        label_description: (pie.label_descriptions !== undefined ? pie.label_descriptions[index] : ''),
        total: total,
        label: pie.labels[index],
        position: (offset > 0 ? 'right' : 'left')
      })
      startAngle += sliceAngle
    })
  }

  defineLines (offset, index, centerX, radius, startAngle, centerY, endAngle) {
    if (offset < 0 && !index) {
      this.lines = []
      this.lines.push({
        start: {
          x: centerX + radius * Math.cos(startAngle),
          y: centerY + radius * Math.sin(startAngle)
        },
        end: {
          x: centerX + (-offset - 10) + radius * Math.cos(Math.PI / 2),
          y: centerY - 1 + radius * Math.sin(Math.PI / 2)
        }
      })
      this.lines.push({
        start: {
          x: centerX + radius * Math.cos(endAngle),
          y: centerY + radius * Math.sin(endAngle)
        },
        end: {
          x: centerX + (-offset - 10) + radius * Math.cos(Math.PI * (180 * Math.PI)),
          y: centerY + 1.5 + radius * Math.sin(Math.PI * (180 * Math.PI))
        }
      })
    }
  }

  drawLines (canvas) {
    this.lines.forEach(line => {
      this.drawPieLine(canvas, line.start.x, line.start.y, line.end.x, line.end.y)
    })
  }
  /**
   * Draw Pie Slice
   * @param centerX
   * @param centerY
   * @param radius
   * @param startAngle
   * @param endAngle
   * @param color
   */
  drawPieSlice ({ctx, matrix}, centerX, centerY, radius, startAngle, endAngle, color) {
    ctx.fillStyle = color
    ctx.beginPath()
    ctx.moveTo(centerX, centerY)
    ctx.arc(centerX, centerY, radius, startAngle, endAngle)
    ctx.closePath()
    ctx.fill()
  }
  drawPieLine ({ctx, matrix}, startX, startY, endX, endY) {
    let color = '#cdcdcd'
    let rgb = this.Utils.hexToRgb(color)
    color = `rgba(${rgb.r},${rgb.g},${rgb.b},0.7)`
    ctx.beginPath()
    ctx.strokeStyle = (matrix ? this.Utils.getRandomColor() : color)
    ctx.moveTo(startX, startY)
    ctx.lineTo(endX, endY)
    ctx.stroke()
    ctx.closePath()
  }
  drawPieSliceLabel ({ctx, matrix}) {
    let ratio = this.layout.height / this.layout.width
    this.pieSlices.forEach((slice, index) => {
      let centerX = (!index || index === 2 ? slice.center_x - (50 * ratio) : slice.center_x)
      let centerY = (!index || index === 2 ? slice.center_y - (20 * ratio) : slice.center_y)
      let labelX = centerX + (slice.radius / 2) * Math.cos(slice.start_angle + slice.slice_angle / 2)
      let labelY = centerY + (slice.radius / 2) * Math.sin(slice.start_angle + slice.slice_angle / 2)
      let labelText = ((slice.value / slice.total) * 100).toFixed(1)
      let color = (matrix ? this.Utils.getRandomColor() : this.labelStyles.color)
      let text = {
        type: 'label',
        key: color,
        color: this.labelStyles.color,
        x: labelX,
        y: labelY,
        text: `${slice.label}`,
        child: [
          {
            text: `${slice.improved_value}, ${labelText} %`,
            x: labelX,
            y: (labelY + 15)
          }
        ]
      }
      if (this.activeObject && this.activeObject.object.color === slice.color) {
        this.drawText(ctx, {...text, color, ...this.labelStyles, bold: 'bold'})
      } else {
        this.drawText(ctx, {...text, color, ...this.labelStyles})
      }
    })
  }

  drawText (ctx, text) {
    let bold = (text.bold !== undefined ? text.bold : '')
    ctx.fillStyle = text.color
    ctx.font = `${bold} ${text.font_size}px ${text.font_family}`
    ctx.fillText(text.text, text.x, text.y)
    if (text.child !== undefined) {
      ctx.fillText(text.child[0].text, text.x, text.y + 15)
    }
  }

  drawLegend ({ctx, matrix}) {
    let offsetX = this.layout.width / 2
    let offsetY = this.layout.height - 5
    let squareWidth = 25
    let squareHeight = 15
    let totalWidth = 0
    let squareLeftOffset = 10
    // calc total Length Width
    this.pieSlices.forEach(entry => {
      if (this.activeObject && this.activeObject.object.color === entry.color) {
        ctx.font = `bold ${this.legendStyles.font_size}px ${this.legendStyles.font_family}`
      } else {
        ctx.font = `${this.legendStyles.font_size}px ${this.legendStyles.font_family}`
      }
      totalWidth += (squareWidth + ctx.measureText(entry.label).width) + squareLeftOffset
    })
    let settings = {squareWidth, squareHeight, offsetX, offsetY, squareLeftOffset}
    if (totalWidth > this.layout.width) {
      this.drawLegendFor({ctx, matrix}, this.pieSlices.slice(0, 2), {...settings, split: true})
      this.drawLegendFor({ctx, matrix}, this.pieSlices.slice(2, 5), {...settings, offsetY: offsetY + 20, split: true})
    } else {
      offsetX = offsetX - (totalWidth / 2)
      this.drawLegendFor({ctx, matrix}, this.pieSlices, {...settings, offsetX})
    }
  }
  drawLegendFor ({ctx, matrix}, pieObjects, {offsetX, offsetY, squareWidth, squareHeight, squareLeftOffset, split = false}) {
    if (split) {
      let totalWidth = 0
      pieObjects.forEach(entry => {
        if (this.activeObject && this.activeObject.object.color === entry.color) {
          ctx.font = `bold ${this.legendStyles.font_size}px ${this.legendStyles.font_family}`
        } else {
          ctx.font = `${this.legendStyles.font_size}px ${this.legendStyles.font_family}`
        }
        totalWidth += (squareWidth + ctx.measureText(entry.label).width) + squareLeftOffset
      })
      offsetX = offsetX - (totalWidth / 2)
    }
    pieObjects.forEach(entry => {
      let color = (matrix ? this.Utils.getRandomColor() : entry.color)
      // draw legend square
      ctx.fillStyle = color
      ctx.fillRect(offsetX, offsetY - 32, squareWidth, squareHeight)
      // draw legend label text
      let textColor = (matrix ? this.Utils.getRandomColor() : this.legendStyles.color)
      let text = {type: 'legend', text: `${entry.label}`, x: offsetX + squareWidth + 5, y: offsetY - 20, color: this.legendStyles.color, key: textColor}
      if (this.activeObject && this.activeObject.object.color === entry.color) {
        this.drawText(ctx, {...text, ...this.legendStyles, color: textColor, bold: 'bold'})
      } else {
        this.drawText(ctx, {...text, ...this.legendStyles, color: textColor})
      }
      offsetX += squareWidth + ctx.measureText(entry.label).width + squareLeftOffset
    })
  }
  drawLabelDescriptions ({ctx, index}) {
    let pieSlice = this.pieSlices[index]
    if (typeof pieSlice === 'object') {
      ctx.font = `bold ${this.labelDescriptionStyles.font_size}px ${this.labelDescriptionStyles.font_family}`
      let textWidth = ctx.measureText(pieSlice.label_description).width
      let positionOnX = (this.layout.width / 2) - (textWidth / 2)
      ctx.fillStyle = this.labelDescriptionStyles.color
      ctx.fillText(pieSlice.label_description, positionOnX, this.layout.height - 40)
    }
  }
  addObject (object, isMatrix) {
    if (isMatrix) {
      switch (object.type) {
        case 'pie': {
          this.matrixObjects.push(object)
          break
        }
        default: {
          this.unknownMatrixObjects.push(object)
        }
      }
    }
  }
}

class ChartUtils {
  static getRandomColor () {
    const r = Math.round(Math.random() * 255)
    const g = Math.round(Math.random() * 255)
    const b = Math.round(Math.random() * 255)
    return `rgb(${r},${g},${b})`
  }
  static componentToHex (c) {
    let hex = c.toString(16)
    return (hex.length === 1 ? '0' + hex : hex)
  }
  static rgbToHex (r, g, b) {
    return '#' + this.componentToHex(r) + this.componentToHex(g) + this.componentToHex(b)
  }
  static hexToRgb (hex) {
    let result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex)
    return result ? {
      r: parseInt(result[1], 16),
      g: parseInt(result[2], 16),
      b: parseInt(result[3], 16)
    } : null
  }
}
