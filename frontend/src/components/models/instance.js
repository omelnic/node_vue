import _ from 'lodash'

export default class Instance {
  constructor ({host, username, password, color, shared}) {
    this.host = host
    this.username = username
    this.password = password
    this.color = color
    this.shared = shared
  }
  toString () {
    return this.host
  }
  isValid () {
    if (this._id && this._id.length) {
      return this.host.length
    }
    return !_.chain(this).pick(['host', 'username', 'password']).every(_.isEmpty).value()
  }
  static properties () {
    return {host: '', username: '', password: '', shared: false, color: '', _id: null}
  }
}
