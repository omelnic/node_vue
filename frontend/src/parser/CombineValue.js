import _ from 'lodash'
export default class CombineValue {
  // > Must get full report response and return full report response
  static parse (report, configuration) {
    if (_.isEmpty(configuration.combine)) {
      return report
    }
    if (report.reports !== undefined && report.reports.length > 0) {
      _.each(report.reports, collection => {
        _.each(collection.data, row => {
          _.each(configuration.combine, (value, property) => {
            if (typeof value === 'function') {
              let args = this.extractFunctionArgs(value)
              let mappedValues = []
              if (args.length) {
                args.forEach(fieldName => (mappedValues.push(row[collection.header.indexOf(fieldName)])))
              }
              row[collection.header.indexOf(property)] = value.apply(this, [row[collection.header.indexOf(property)], ...mappedValues])
            } else {
              row[collection.header.indexOf(property)] = value.format(row[collection.header.indexOf(property)])
            }
          })
        })
      })
    }
    return report
  }
  /**
   * Extract function args of field
   * @param func
   * @return {Array}
   */
  static extractFunctionArgs (func) {
    let pattern = /function[^(]*\(([^)]*)\)/
    let args = func.toString().match(pattern)[1].split(/,\s*/)
    if (args[0].toLowerCase() === 'value') {
      delete args[0]
    }
    return _.compact(args)
  }
}
