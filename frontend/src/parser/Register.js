import _ from 'lodash'
import CombineValue from './CombineValue'
import Dummy from './DummyParser'
import MissingColumns from './MissingColumnsParser'
import RenamedColumns from './RenamedColumns'

// > Just class to keep all parsers access in one place
class Register {
  constructor () {
    this.register = {}
  }
  registerParser (name, parser) {
    this.register[name] = parser
    return this
  }
  findParserByName (name) {
    return this.register[name]
  }
  hasParser (name) {
    return _.has(this.register, name)
  }
}

export default new Register()
  .registerParser('DummyParser', Dummy)
  .registerParser('MissingColumns', MissingColumns)
  .registerParser('RenamedColumns', RenamedColumns)
  .registerParser('CombineValue', CombineValue)
