import _ from 'lodash'

export default class ParserMissingHeaders {
  // > Must get full report response and return full report response
  static parse (report, configuration) {
    let columns = configuration.columns
    let process = _.head(report.reports)
    let diff = _.difference(process.header, columns)
    // > If columns a equal, then nothing to do
    if (!diff.length) {
      return report
    }
    // > Append headers missing headers
    process.header = _.union(process.header, columns)
    // > Append missing data
    _.each(process, (row) => { row.concat(Array(process.header - row.length)) })
    // > Return modified report
    return report
  }
}
