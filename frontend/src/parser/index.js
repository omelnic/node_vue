import _ from 'lodash'
import Register from './Register'
import Configuration from './Configuration'

export default class Parser {
  static mapArray = function (data, header) {
    let collection = []
    try {
      for (let item in data) {
        let entry = data[item]
        if (entry.length !== header.length) { return [] }
        collection.push(_.zipObject(header, entry))
      }
      return collection
    } catch (e) {
      console.warn('Failed to parse report content', e)
      return collection
    }
  }
  static parse (report, endpoint) {
    try {
      let configuration = Configuration[endpoint]
      if (!configuration) {
        console.error('No configuration found for endpoint ', endpoint)
        // > At least mapped
        return Parser.mapArray(report.data, report.header)
      }
      if (configuration['parser'] !== undefined && configuration['parser'] !== null) {
        for (let i = 0; i < configuration['parser'].length; i++) {
          let nextParser = configuration['parser'][i]
          if (!Register.hasParser(nextParser)) {
            console.error('Register parser do not know parser ', nextParser, ', will skip it')
            continue
          }
          try {
            report = Register.findParserByName(nextParser).parse(report, configuration)
          } catch (err) {
            console.error(err)
          }
        }
      }
      return Parser.mapArray(_.head(report.reports).data, _.head(report.reports).header)
    } catch (e) {
      console.error('Can not create parse ', e)
    }
    return null
  }
}
