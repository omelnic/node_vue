// > Parsers and engines for the versions
export default {
  defaults: {
    ts: Date,
    prefix: '',
    asnumber: 0,
    asname: 'As name not found in database'
  },
  '/irp/reports/currentImprovements': {
    parser: ['MissingColumns', 'RenamedColumns'],
    columns: ['ts', 'prefix', 'asnumber', 'asname', 'providerfrom', 'providerto', 'type', 'improvementage', 'probingsource',
      'probingdirection', 'details', 'rd', 'providerfromnexthop', 'providertonexthop', 'lossold', 'latencyold', 'lossnew',
      'latencynew', 'isannounced', 'bgpprefmon'],
    renamed: []
  },
  '/irp/reports/platformOverview': {
    parser: ['MissingColumns', 'RenamedColumns'],
    columns: [
      'totalimprovements',
      'lossimprovements',
      'lossimprovementsby20percentormore',
      'lossimprovementseliminatedloss',
      'lossimprovementsbypassedblackout',
      'latencyimprovements',
      'latencyimprovementsby20percentormore',
      'latencyimprovementsby50percentormore',
      'outagedetectionimprovementsbypassedcongestion',
      'outagedetectionimprovementsbypassedoutage',
      'reducedcost',
      'adjustedcommitlevels',
      'improvedinboundprefixes',
      'totalinboundimprovements',
      'performanceinboundimprovements',
      'commitinboundimprovements',
      'manualinboundimprovements'
    ],
    renamed: []
  },
  '/irp/reports/providersPerformance': {
    parser: ['MissingColumns', 'RenamedColumns'],
    columns: ['idprovider', 'description', 'provider', 'latency', 'loss', 'cntlatency', 'cntloss', 'cnttotal'],
    renamed: []
  },
  '/irp/reports/latencyImprovements': {
    parser: ['MissingColumns', 'RenamedColumns'],
    columns: ['label', 'totalold', 'old', 'new'],
    renamed: []
  },
  '/irp/reports/probedPrefixesAndImprovements': {
    parser: ['MissingColumns', 'RenamedColumns'],
    columns: ['ts', 'probedprefixes', 'losscnt', 'latencycnt', 'commitcontrolcnt', 'othercnt'],
    renamed: []
  },
  '/irp/reports/ASPatternProblems': {
    reportPage: '/reports/as-pattern-problems',
    parser: ['MissingColumns', 'RenamedColumns', 'CombineValue'],
    columns: ['ts', 'problemid', 'aspattern', 'problem', 'total', 'selectedforprobing', 'probed', 'pendingprobing',
      'problemconfirmed', 'problemnotconfirmed', 'confirmationrate', 'problemstatus'
    ],
    renamed: [],
    combine: {
      problemnotconfirmed: function (value, problemid, aspattern) {
        return {
          name: 'ReportAsPatternProblems',
          query: {prefixState: 0, problemid, aspattern},
          params: {report: 'ASPatternProblemPrefixes', view: 'table-report'},
          view: 'table-report',
          value
        }
      },
      problemconfirmed: function (value, problemid, aspattern) {
        return {
          name: 'ReportAsPatternProblems',
          query: {prefixState: 2, problemid, aspattern},
          params: {report: 'ASPatternProblemPrefixes', view: 'table-report'},
          value
        }
      },
      pendingprobing: function (value, problemid, aspattern) {
        return {
          name: 'ASPatternProblemPrefixes',
          query: {prefixState: 1, problemid, aspattern},
          params: {report: 'ASPatternProblemPrefixes', view: 'table-report'},
          value
        }
      },
      probed: function (value, problemid, aspattern) {
        return {
          name: 'ASPatternProblemPrefixes',
          query: {prefixState: '0,2', problemid, aspattern},
          params: {report: 'ASPatternProblemPrefixes', view: 'table-report'},
          value
        }
      },
      selectedforprobing: function (value, problemid, aspattern) {
        return {
          name: 'ASPatternProblemPrefixes',
          query: {prefixState: '0,1,2', problemid, aspattern},
          params: {report: 'ASPatternProblemPrefixes', view: 'table-report'},
          value
        }
      },
      confirmationrate: value => (`${value} %`),
      total: function (value, problemid, aspattern) {
        return {
          name: 'ASPatternProblemPrefixes',
          query: {problemid, aspattern},
          params: {report: 'ASPatternProblemPrefixesState', view: 'table-report'},
          value
        }
      }
    }
  },
  '/irp/reports/ASPatternProblemPrefixes': {
    reportPage: '/reports/as-pattern-problems/prefixes',
    parser: ['MissingColumns', 'RenamedColumns', 'CombineValue'],
    columns: ['ts', 'problemid', 'aspattern', 'problem', 'prefix', 'prefixstate'],
    renamed: []
  }
}
