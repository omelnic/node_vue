// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import VueConfig from './config/index.config'
import App from './App'
import router from './router'
import store from './store'
import VTooltip from 'v-tooltip'
import VueMasonry from 'vue-masonry-css'
import format from 'string-format'
import KeenUI from 'keen-ui'
import validator from 'simple-vue-validator'
import VueContentPlaceholders from 'vue-content-placeholders'
import ClientSocket from './config/client_socket'
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueAuth from '@websanova/vue-auth'

Vue.use(...ClientSocket(store))
Vue.use(VueConfig)
Vue.use(VTooltip)
Vue.use(VueContentPlaceholders)
Vue.use(validator)
Vue.use(KeenUI)
Vue.use(VueMasonry)
Vue.use(VueAxios, axios)

const port = window.location.port
const URL = window.location.protocol + '//' + window.location.hostname + (port ? ':' + port : '') + '/'
Vue.axios.defaults.baseURL = URL

Vue.router = router

Vue.use(VueAuth, {
  router: require('@websanova/vue-auth/drivers/router/vue-router.2.x.js'),
  http: require('@websanova/vue-auth/drivers/http/axios.1.x.js'),
  auth: require('@websanova/vue-auth/drivers/auth/bearer.js'),
  rolesVar: 'role'
})

Vue.directive('vue-tooltip', VTooltip)
format.extend(String.prototype)

// Start
var component = App
component.router = Vue.router
component.store = store
component.template = '<App/>'

new Vue(component).$mount('#app')
/* eslint-disable no-new */
// new Vue({
//   el: '#app',
//   router,
//   store,
//   template: '<App/>',
//   components: { App }
// })
