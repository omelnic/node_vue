import socketio from 'socket.io-client'
import VueSocketIO from 'vue-socket.io'
import Vue from 'vue'
let io = socketio(`${Vue.config.WEB_SOCKET_HOST()}`, Vue.config.WS_OPTIONS)

export default (store) => {
  return [VueSocketIO, io, store]
}
