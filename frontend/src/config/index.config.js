import VueDevConfig from './dev.config'
import VueProdConfig from './prod.config'
import Vue from 'vue'
import _ from 'lodash'

const initialised = {
  config () {
    Vue.config.HAS_ACTIVE_TAB = true
    window.addEventListener('focus', () => (Vue.config.HAS_ACTIVE_TAB = true))
    window.addEventListener('blur', () => (Vue.config.HAS_ACTIVE_TAB = false))
    if (process.env.NODE_ENV === 'development') {
      _.assign(Vue.config, VueDevConfig)
    } else {
      _.assign(Vue.config, VueProdConfig)
    }
    return Vue.config
  }
}

export default initialised.config()
