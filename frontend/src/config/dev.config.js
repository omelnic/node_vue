export default {
  is_prod: false,
  productionTip: false,
  HOST: window.location.hostname,
  WEB_SOCKET_PORT: 10080,
  WEB_SOCKET_PROTOCOL: 'ws://',
  WS_OPTIONS: {
    path: '/',
    transports: ['websocket']
  },
  WEB_SOCKET_HOST: function () {
    return this.WEB_SOCKET_PROTOCOL + this.HOST + ':' + this.WEB_SOCKET_PORT
  }
}
