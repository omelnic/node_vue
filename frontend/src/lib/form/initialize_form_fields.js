import FieldOptionsFactory from './field_options_factory'
import _ from 'lodash'

export default class InitializeFormFields {
  fields = {}
  settings = {}
  fieldsWithOptions = {}
  fieldsWithSelectedOptions = {}
  fieldsWithSelectedOptionValue = {}
  constructor (fields, settings = {}) {
    if (!fields) {
      throw new Error('Missing requird settings')
    }
    this.fields = fields
    this.settings = settings
    this.render()
  }
  render () {
    let auxSettings = []
    let formFieldSettings = null
    _.each(this.fields, (selected, key) => {
      try {
        if (typeof this.settings[key] === 'object') {
          auxSettings = this.settings[key]
        }
        formFieldSettings = {selected: selected, ...auxSettings}
        let field = new FieldOptionsFactory(key, formFieldSettings).getFieldObject()
        this.fields[key] = field
        this.fieldsWithOptions[key] = (typeof field === 'object' ? field.getOptions() : field)
        this.fieldsWithSelectedOptions[key] = (typeof field === 'object' ? field.getSelected() : field)
        this.fieldsWithSelectedOptionValue[key] = (typeof field === 'object' ? field.getSelectedValues() : field)
      } catch (err) {
        console.error(err)
        this.fields[key] = selected
        this.fieldsWithOptions[key] = selected
        this.fieldsWithSelectedOptions[key] = selected
        this.fieldsWithSelectedOptionValue[key] = selected
      }
    })
  }
  getFields () {
    return this.fields
  }
  getFieldsWithOptions () {
    return this.fieldsWithOptions
  }
  getSelectedOptions () {
    return this.fieldsWithSelectedOptions
  }
  getInitialSelectedOptionValues () {
    return this.fieldsWithSelectedOptionValue
  }
  extractFieldValues (fieldsSettings) {
    let selectedFieldValues = {}
    _.each(fieldsSettings, (field, key) => {
      if (_.isArray(field)) {
        field.value = _.map(field, entity => {
          return entity.value
        })
      }
      selectedFieldValues[key] = field.value
    })
    return selectedFieldValues
  }
}
