import _ from 'lodash'

/**
 * BaseFieldOptions class match and map options get from child Class and convert to keen-ui format for select-ui [{label: '', value: ''}]
 */
export default class BaseFieldOptions {
  isMultiSelect = false
  constructor (settings = {selected: null, all: true, options: null}) {
    this.settings = settings
  }
  fillOptions (options) {
    if (!_.isArray(this.getSettings().options)) {
      this.getSettings().options = options
    }
  }
  getSettings () {
    return this.settings
  }
  /**
   * Get Selected method return default selected option from the list of the options based on field {settings: selected: value}
   * The gived value is into keen-ui format for select-ui {label: name, value: val}
   * @return {*}
   */
  getSelected () {
    if (this.isMultiSelect) {
      return this.getMultiSelected()
    }
    if (this.getSettings().selected === null || this.getSettings().selected === 'undefined') {
      return _.head(this.getOptions())
    }
    return _.find(this.getOptions(), ['value', this.getSettings().selected])
  }
  /**
   * Get Selected Value return the selected Value for the field not in keen-ui format strict value
   */
  getSelectedValues () {
    if (this.isMultiSelect) {
      return this.getMultiSelectedValues()
    }
    return this.getSelected().value
  }
  /**
   * Provide the Multiple selected values
   * @return {*}
   */
  getMultiSelected () {
    if (!this.isMultiSelect) {
      return super.getSelected()
    }
    if (this.getSettings().selected === null || this.getSettings().selected === 'undefined') {
      return [_.head(this.getOptions())]
    }
    if (_.isArray(this.getSettings().selected)) {
      let selected = _.map(this.getOptions(), option => (_.includes(this.getSettings().selected, option.value) ? option : false))
      return (selected ? _.compact(selected) : [])
    }
    return [_.find(this.getOptions(), ['value', this.getSettings().selected])]
  }
  getMultiSelectedValues () {
    if (!this.isMultiSelect) {
      return super.getSelectedValues()
    }
    if (this.getSettings().selected === null || this.getSettings().selected === 'undefined') {
      return [_.head(this.getOptions()).value]
    }
    if (_.isArray(this.getSettings().selected)) {
      let selected = _.map(this.getOptions(), option => (_.includes(this.getSettings().selected, option.value) ? option.value : false))
      return (selected ? _.compact(selected) : [])
    }
    return [_.find(this.getOptions(), ['value', this.getSettings().selected]).value]
  }
  getOptions () {
    return this.getSettings().options
  }
}
