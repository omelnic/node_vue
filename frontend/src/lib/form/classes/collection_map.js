import _ from 'lodash'
import BaseFieldOptions from './base_field_options'

export default class CollectionMap extends BaseFieldOptions {
  constructor (settings = {selected: null, label: null, value: null, collection: null}) {
    if (!settings.label) {
      throw new Error('Missing required field for label')
    }
    if (!settings.label) {
      throw new Error('Missing required field for value')
    }
    super(settings)
    this.matchSettings()
    this.mapFieldOptions(settings.collection)
    this.fillOptions(this.getMappedFieldOptions())
  }
  /**
   * Match Settings
   */
  matchSettings () {
    if (typeof this.getSettings().label === 'string') {
      this.settings.label = [this.getSettings().label]
    }
    if (typeof this.getSettings().value === 'string') {
      this.settings.value = [this.getSettings().value]
    }
    if (!_.isArray(this.settings.label)) {
      throw new Error('Missing required `label` attrubute')
    }
    if (!_.isArray(this.settings.value)) {
      throw new Error('Missing required `value` attribute')
    }
  }
  getMappedFieldOptions () {
    return this.options
  }
  mapFieldOptions (collection) {
    if (!collection) {
      throw new Error('Missing Required Collection')
    }
    this.options = _.map(collection, entity => {
      let label = _.reduce(_.pick(entity, this.getSettings().label), (result, value) => {
        return (result ? result + '|' + value : value)
      })
      let value = _.reduce(_.pick(entity, this.getSettings().value), (result, value) => {
        return (result ? result + '_' + value : value)
      })
      return {label: label, value: value}
    })
  }
}
