import BaseFieldOptions from './classes/base_field_options'
import CollectionMap from './classes/collection_map'
import _ from 'lodash'

export default class FieldOptionsFactory {
  constructor (FormOption = 'FieldOptions', settings = null) {
    if (!FormOption) {
      throw new Error('Missing Required FormFieldOption')
    }
    // eslint-disable-next-line no-eval
    FormOption = eval(FormOption)
    if (typeof FormOption === 'function') {
      if (!_.isEmpty(settings)) {
        this.fieldObject = new FormOption(settings)
      } else {
        this.fieldObject = new FormOption()
      }
    }
  }
  getFieldObject () {
    return this.fieldObject
  }
}

export class RefreshOptions extends BaseFieldOptions {
  constructor (settings = {selected: 0, options: null}) {
    super(settings)
    this.fillOptions([
      {label: 'Never', value: 0},
      {label: 'Every 20 seconds', value: 20},
      {label: 'Every 1 minute', value: 60},
      {label: 'Every 5 minute', value: 300},
      {label: 'Every 15 minute', value: 900},
      {label: 'Every 30 minute', value: 1800},
      {label: 'Every 1 hour', value: 1800}
    ])
  }
}

export class PerInstanceOptions extends BaseFieldOptions {
  constructor (settings = {selected: 2, options: null}) {
    super(settings)
    this.fillOptions([
      {label: '2 last records', value: 2},
      {label: '5 last records', value: 5},
      {label: '10 last records', value: 10},
      {label: '15 last records', value: 15},
      {label: '20 last records', value: 20}
    ])
  }
}

export class IpAllOptions extends BaseFieldOptions {
  constructor (settings = {selected: 0, all: true, options: null}) {
    super(settings)
    this.fillOptions([
      {label: 'All', value: 0},
      {label: 'IPv4', value: 4},
      {label: 'IPv6', value: 6}
    ])
  }
}

export class TimestampInterval extends BaseFieldOptions {
  constructor (settings = {selected: 0, all: true, options: null}) {
    super(settings)
    this.fillOptions([
      {label: 'Last 4 hours', value: 4},
      {label: 'Last 12 hours', value: 12},
      {label: 'Last 24 hours', value: 24}
    ])
  }
}

export class IpOptions extends IpAllOptions {
  constructor (settings = {}) {
    super(settings)
  }
  getOptions () {
    let options = _.map(super.getOptions(), (element) => {
      return (element.value > 0 ? element : null)
    })
    return _.compact(options)
  }
}

export class ComponentsOptions extends BaseFieldOptions {
  isMultiSelect = true
  constructor (settings = {selected: 'Irpspand', options: null}) {
    super(settings)
    this.fillOptions([
      {label: 'SPANd', value: 'Irpspand'},
      {label: 'Core', value: 'Core'},
      {label: 'BGPd', value: 'Explorer'},
      {label: 'FLOWd', value: 'BGPd'},
      {label: 'APId', value: 'Irpapid'},
      {label: 'PUSHd', value: 'Irppushd'},
      {label: 'Failover', value: 'Failover'},
      {label: 'ConfigSync', value: 'ConfigSync'}
    ])
  }
}

export class Region extends BaseFieldOptions {
  constructor (settings = {selected: 'total', options: null}) {
    super(settings)
    this.fillOptions([
      {label: 'Total', value: 'total'},
      {label: 'Local', value: 'local'},
      {label: 'Regional', value: 'regional'},
      {label: 'Continental', value: 'continental'},
      {label: 'Transcontinental', value: 'transcontinental'},
      {label: 'Global', value: 'global'}
    ])
  }
}

export class ProblemType extends BaseFieldOptions {
  constructor (settings = {selected: 'sumloss', options: null}) {
    super(settings)
    this.fillOptions([
        {label: 'Loss', value: 'sumloss'},
        {label: 'RTT', value: 'sumrttavg'},
        {label: 'Probes', value: 'cntprobes'},
        {label: 'Failed Probes', value: 'cntprobesfailed'}
    ])
  }
}

/**
 * Create custom field options using Collections
 */
export class Instance extends CollectionMap {
  constructor (settings = {selected: 0, label: null, value: null, collection: null}) {
    super(settings)
    this.fillOptions(settings.options)
  }
}

export class Instances extends CollectionMap {
  isMultiSelect = true
  constructor (settings = {selected: 0, label: null, value: null, collection: null}) {
    super(settings)
    this.fillOptions(settings.options)
  }
}
