export default class extends Error { // (1)
  constructor (response) {
    if (response.data && response.data.error) {
      super(response.data.message)
    } else {
      super(`${response.status} for ${response.url}`)
    }
    this.name = 'HttpError'
    this.response = response
  }
}
