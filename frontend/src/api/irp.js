import _ from 'lodash'
import { Axios } from 'axios'

let extractReport = function (response) {
  try {
    if (response.status !== 200 && response.statusText !== 'OK') {
      return {}
    }
    let results = response.data.results[0]
    let reports = response.data.reports[0]
    if (results.code === 'ok' && reports) {
      return reports
    }
  } catch (e) {}
  return response.data
}

let mapArray = function (data, header) {
  var collection = []
  try {
    for (var item in data) {
      var entry = data[item]
      if (entry.length !== header.length) { return [] }
      collection.push(_.zipObject(header, entry))
    }
    return collection
  } catch (e) {
    console.warn('Failed to parse report content', e)
    return collection
  }
}

let responseParser = function (response) {
  if (response.config.map) {
    let report = extractReport(response)
    if (report) {
      response.filters = report.filters
      response.header = report.header || report.fields
      response.title = report.title
      response.type = report.type
      response.records = report.records
      response.data = mapArray(report.data, report.header || report.fields)
    }
  }
  return response
}

export default class extends Axios {
  constructor (hosts) {
    const headers = { 'Content-Type': 'application/json' }
    headers['X-IRP-HOSTS'] = hosts
    super({headers})
    this.interceptors.response.use(responseParser)
  }
}
