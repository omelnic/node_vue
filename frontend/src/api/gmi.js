import _ from 'lodash'
import axios from 'axios'
import HttpError from './exception'

export default {
  handleError: function (error) {
    console.error(error)
    return []
  },
  getInstances () {
    return axios.get('/gmi/instances')
      .then((response) => { return response })
      .catch(this.handleError)
  },

  addInstance (instance) {
    return axios.post('/gmi/instances', instance)
      .then(function (response) {
        if (response.data && response.data.error) {
          throw new HttpError(response)
        }
        instance = _.extend(instance, response.data)
        return {response, instance}
      })
  },

  signUp (username, password, instances) {
    return axios.post('/gmi/signup', {username, password, instances})
      .then(function (response) {
        if (response.data && response.data.error) {
          throw new HttpError(response)
        }
        instances = _.extend(instances, response.data)
        return {response, instances: instances}
      })
  },

  saveInstance (instance) {
    return axios.put('/gmi/instances/{}'.format(instance._id), instance)
      .then(function (response) {
        instance = _.extend(_.omit(instance, ['password']), response.data)
        return {response, instance}
      })
  },
  removeInstance (instance) {
    return axios.delete('/gmi/instances/{}'.format(instance._id))
      .then(function (response) {
        if (response.data && response.data.error === true) {
          throw new HttpError(response)
        }
        return {response, instance}
      })
  },
  addWidget (dashboard, widget) {
    return axios.post('/gmi/dashboards/{}/widgets'.format(dashboard._id), widget)
      .then(function (response) {
        if (response.data && response.data.error) {
          throw new HttpError(response)
        }
        widget = _.extend(widget, response.data)
        return {response, widget}
      })
  },
  updateWidget (dashboard, widget) {
    return axios.put('/gmi/dashboards/{}/widgets/{}'.format(dashboard._id, widget._id), widget)
      .then(function (response) {
        if (response.data && response.data.error) {
          throw new HttpError(response)
        }
        widget = _.extend(widget, response.data)
        return {response, widget}
      })
  },
  deleteWidget (dashboard, widget) {
    return axios.delete('/gmi/dashboards/{}/widgets/{}'.format(dashboard._id, widget._id), widget)
      .then(function (response) {
        if (response.data && response.data.error) {
          throw new HttpError(response)
        }
        widget = _.extend(widget, response.data)
        return {response, widget}
      })
  }
}
