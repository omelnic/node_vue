#!/bin/bash

#     usage :
#
#     edit two vars in below :  NODE_VER  and  CODE_PARENT_DIR
#
# ... execute this script as yourself unless you choose a root owned value for var CODE_PARENT_DIR
#     whichever id you execute this as determines the id you will issue npm commands as :  npm install -g foo-bar
#
#     NOTE - nodejs comes bundled with npm ... so no need to do separate npm install
#            this scripts runs fine on linux or OSX

# ...  copy all the lines starting here  .. top of copy  ....   and ending ... end of copy  ...
#      and paste into your ~/.bashrc file so proper env vars get set

# ............... top of copy  ........................  install_node.sh

# export NODE_VER=v7.2.0  # see available versions at https://nodejs.org/dist/
export NODE_VER=v8.5.0  # edit this line next time you need to update nodejs

# ...  pick parent dir of nodejs install  ... comment out or remove ONE of below
# export CODE_PARENT_DIR=/opt/code  # root owned dir ... requires you to sudo prior to npm install going forward
export CODE_PARENT_DIR=${HOME}    # RECOMMENDED execute as yourself including npm install

# ......... following env vars are OK no edits needed ... only ever need to edit above vars

curr_OS=$( uname )

echo curr_OS $curr_OS

if [[ "${curr_OS}" == "Darwin" ]]; then

    OS_ARCH=darwin-x64

elif [[ "${curr_OS}" == "Linux" ]]; then

    OS_ARCH=linux-x64
else
    echo "ERROR - failed to recognize OS $curr_OS"
    exit 5
fi

if [[ -z ${CODE_PARENT_DIR} ]]; then

    echo "ERROR - failed to see env var CODE_PARENT_DIR"
    exit 5
fi

export NODE_CODEDIR=${CODE_PARENT_DIR}/nodejs
export COMSUFFIX=tar.gz
export NODE_NAME=node-${NODE_VER}
export NODE_PARENT=${NODE_CODEDIR}/${NODE_NAME}-${OS_ARCH}

export PATH=${NODE_PARENT}/bin:${PATH}
export NODE_PATH=${NODE_PARENT}/lib/node_modules

# ............... end of copy  ........................  install_node.sh

# copy and paste above from ... top of copy ... to here into your file ~/.bashrc

echo
echo "NODE_CODEDIR $NODE_CODEDIR<--"
echo

echo "mkdir -p ${NODE_CODEDIR}"
echo
      mkdir -p ${NODE_CODEDIR}
echo

echo "cd ${NODE_CODEDIR}"
      cd ${NODE_CODEDIR}
echo

# this is compiled code NOT source

[ -f ${NODE_NAME}-${OS_ARCH}.${COMSUFFIX} ] && rm ${NODE_NAME}-${OS_ARCH}.${COMSUFFIX} # if file exists remove

echo "wget -q --show-progress https://nodejs.org/download/release/${NODE_VER}/${NODE_NAME}-${OS_ARCH}.${COMSUFFIX}"
      wget -q --show-progress https://nodejs.org/download/release/${NODE_VER}/${NODE_NAME}-${OS_ARCH}.${COMSUFFIX}
echo

echo "tar -C ${NODE_CODEDIR} -xf ${NODE_NAME}-${OS_ARCH}.${COMSUFFIX}"
      tar -C ${NODE_CODEDIR} -xf ${NODE_NAME}-${OS_ARCH}.${COMSUFFIX}
echo

[ -f ${NODE_NAME}-${OS_ARCH}.${COMSUFFIX} ] && rm ${NODE_NAME}-${OS_ARCH}.${COMSUFFIX} # if file exists remove

# ...........  done ........... #

which node

node --version

# ....  bottom of file   install_node.sh
